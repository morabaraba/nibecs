
import { Position, Velocity, Rotation } from 'components/movement'
import { Sprite } from 'components/entity'
import { Input } from 'components/input'
import { Vector3 } from 'types'

// A generic proxy class that takes a store and an eid as arguments
class GenericProxy {
    constructor(store, eid) {
        this.eid = eid;
        this.store = store;
    }
}

// A function that scans the keys of a component and adds getters and setters to the proxy class
function addGettersAndSetters(component, proxyClass) {
    // Get the keys of the component
    const keys = Object.keys(component)
    // Loop through the keys
    for (let key of keys) {
        // Define a getter for the key
        Object.defineProperty(proxyClass.prototype, key, {
            get: function () {
                return this.store[key][this.eid]
            },
            set: function (val) {
                this.store[key][this.eid] = val
            },
        })
    }
}

// Define the InputProxy class that extends the Proxy class
class InputProxy extends GenericProxy {
    constructor(eid) { super(Input, eid) }
  }
// Call the function to add getters and setters to the InputProxy class
addGettersAndSetters(Input, InputProxy)

class Vector3Proxy extends GenericProxy { }
addGettersAndSetters(Vector3, Vector3Proxy)

class PositionProxy extends Vector3Proxy {
    constructor(eid) { super(Position, eid) }
}

class RotationProxy extends Vector3Proxy {
    constructor(eid) { super(Rotation, eid) }
}

class VelocityProxy extends Vector3Proxy {
    constructor(eid) { super(Velocity, eid) }
}

class SpriteProxy extends GenericProxy {
    constructor(eid) { super(Sprite, eid) }
  }
addGettersAndSetters(Sprite, SpriteProxy)

export {
    Vector3Proxy, PositionProxy, VelocityProxy, InputProxy, SpriteProxy, RotationProxy
}