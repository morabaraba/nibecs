// Assuming you have the javascript-state-machine library included
// import StateMachine from 'javascript-state-machine';

/**
 * @class
 * Base class for all soccer player states.
 */
class BasePlayerState {
  /**
   * The finite state machine instance.
   * @type {StateMachine}
   */
  fsm;
  /**
   * The sprite associated with the player state.
   * @type {object}
   */
  sprite;
  /**
   * The flag to enable or disable FSM validation.
   * @type {boolean}
   */
  validateFsm;

  constructor(fsmConfig, sprite, validateFsm = false) {
      this.sprite = sprite;
      this.validateFsm = validateFsm;
      fsmConfig.events = fsmConfig.events || [{ name: 'ending', from: 'idle', to: 'end' }];
      this.fsm = StateMachine.create({
          initial: fsmConfig.initial || 'idle',
          events: [
              { name: 'start', from: 'idle', to: 'moving' },
              { name: 'move', from: 'moving', to: 'playing' },
              { name: 'stop', from: 'playing', to: 'idle' },
              ...fsmConfig.events // Append additional events from derived classes
          ],
          callbacks: {
              onenterstate: this.onEnterState.bind(this),
              onexitstate: this.onExitState.bind(this),
              onvalidateerror: this.onValidateError.bind(this)
          }
      });
  }

  /**
   * Method called on entering a state.
   * @param {string} event - The event name.
   * @param {string} from - The state transitioned from.
   * @param {string} to - The state transitioned to.
   */
  onEnterState(event, from, to) {
      console.log(`Event [${event}] transition from [${from}] to [${to}]`);
  }

  /**
   * Method called on exiting a state.
   * @param {string} event - The event name.
   * @param {string} from - The state transitioned from.
   * @param {string} to - The state transitioned to.
   */
  onExitState(event, from, to) {
      console.log(`Event [${event}] exiting state [${from}] to [${to}]`);
  }

  /**
   * Method called on FSM validation error.
   * @param {Error} error - The error object.
   * @param {string} event - The event name.
   * @param {string} from - The state transitioned from.
   * @param {string} to - The state transitioned to.
   */
  onValidateError(error, event, from, to) {
      console.debug(`[${new Date().toISOString()}] ValidationError: ${error.message}, Event: ${event}, From: ${from}, To: ${to}`);
  }

  /**
   * Method to safely trigger FSM events with validation.
   * @param {string} event - The event name.
   */
  safeTrigger(event) {
      if (this.validateFsm) {
          const eventExists = this.fsm.transitions().includes(event);
          if (eventExists) {
              this.fsm[event]();
          } else {
              this.onValidateError(new Error('Event not found'), event, this.fsm.current, 'unknown');
          }
      } else {
          this.fsm[event]();
      }
  }
}

/**
* @class
* State machine for a Forward player.
* @extends {BasePlayerState}
*/
class Forward extends BasePlayerState {
  constructor(sprite, validateFsm = false) {
      const fsmConfig = {
          initial: 'idle',
          events: [
              { name: 'attack', from: 'idle', to: 'attacking' },
              { name: 'defend', from: 'attacking', to: 'idle' }
          ]
      };
      super(fsmConfig, sprite, validateFsm);
  }
}

/**
* @class
* State machine for a Midfielder player.
* @extends {BasePlayerState}
*/
class Midfielder extends BasePlayerState {
  constructor(sprite, validateFsm = false) {
      const fsmConfig = {
          initial: 'idle',
          events: [
              { name: 'pass', from: 'idle', to: 'passing' },
              { name: 'receive', from: 'passing', to: 'playing' }
          ]
      };
      super(fsmConfig, sprite, validateFsm);
  }
}

/**
* @class
* State machine for a Defender player.
* @extends {BasePlayerState}
*/
class Defender extends BasePlayerState {
  constructor(sprite, validateFsm = false) {
      const fsmConfig = {
          initial: 'idle',
          events: [
              { name: 'block', from: 'idle', to: 'blocking' },
              { name: 'retreat', from: 'blocking', to: 'idle' }
          ]
      };
      super(fsmConfig, sprite, validateFsm);
  }
}

/**
* @class
* State machine for a Goalkeeper player.
* @extends {BasePlayerState}
*/
class Goalkeeper extends BasePlayerState {
  constructor(sprite, validateFsm = false) {
      const fsmConfig = {
          initial: 'idle',
          events: [
              { name: 'catch', from: 'idle', to: 'catching' },
              { name: 'release', from: 'catching', to: 'idle' }
          ]
      };
      super(fsmConfig, sprite, validateFsm);
  }
}

/**
* @class
* SoccerPlayer class that selects the appropriate state machine.
*/
class SoccerPlayer {
  /**
   * The position of the soccer player.
   * @type {string}
   */
  teamPosition;

  /**
   * The state machine instance.
   * @type {BasePlayerState}
   */
  stateMachine;

  /**
   * @param {string} teamPosition - The position of the soccer player.
   * @param {object} sprite - The sprite associated with the soccer player.
   * @param {boolean} validateFsm - Flag to enable or disable FSM validation.
   */
  constructor(teamPosition, sprite, validateFsm = false) {
      this.teamPosition = teamPosition;
      this.sprite = sprite;
      this.validateFsm = validateFsm;
      this.setStateMachine();
  }

  /**
   * Sets the state machine based on the player's position.
   */
  setStateMachine() {
      switch (this.teamPosition) {
          case 'forward':
              this.stateMachine = new Forward(this.sprite, this.validateFsm);
              break;
          case 'midfielder':
              this.stateMachine = new Midfielder(this.sprite, this.validateFsm);
              break;
          case 'defender':
              this.stateMachine = new Defender(this.sprite, this.validateFsm);
              break;
          case 'goalkeeper':
              this.stateMachine = new Goalkeeper(this.sprite, this.validateFsm);
              break;
          default:
              throw new Error('Invalid team position');
      }
  }
}
