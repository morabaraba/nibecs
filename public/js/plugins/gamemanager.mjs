/** global Phaser */
import { applyMixins } from 'utils'

import GamePlayerMixin from "mixins/game/player"
import GameEntityMixin from "mixins/game/entity"
import GameWorldMixin from "mixins/game/world"

const MAX_INT32 = 2147483647

let plugin // Singleton Class forward declaration to get rid of this
let scene
/**
 * @class GameManagerPlugin
 * @extends Phaser.Plugins.ScenePlugin
 * @description Plugin to manage game entities and players using DataManager and EventEmitter.
 */
class BaseGameManagerPlugin extends Phaser.Plugins.ScenePlugin {
	/**
	 * @param {Phaser.Scene} scene - The scene that owns the plugin.
	 * @param {Phaser.Plugins.PluginManager} pluginManager - The plugin manager that manages the plugin.
	 * @param {string} pluginName - The name of the plugin.
	 */
	constructor(scene_, pluginManager, pluginName) {
		super(scene_, pluginManager, pluginName)
		scene = scene_
		console.debug('BaseGameManagerPlugin Constructor, calling superMixins', arguments)
		this.superMixins(arguments)
		console.debug('BaseGameManagerPlugin superMixins done')
		this.secondsElapsed = 0
	}

	/**
	 * @description called when the plugin is first installed or started.
	 */
	boot() {
		plugin = this
		// Initialize DataManager items
		const fps = 48
		plugin.scene.data.set('state', {
			simTickTime: 1000 / fps, // Renamed to simTickTime
			processTickTime: 1000 / fps, // Local games this will stay constant and probably match simTickTime
			processElapsedTime: 0,
			tickLatency: 170,
			tick: 0,
			recvQ: [],
			processQ: [],
			renderQ: []
		})

		plugin.scene.data.set('sprites', [])
		plugin.scene.data.set('players', {})

		// Set state as a class property
		plugin.state = plugin.scene.data.get('state')
		plugin.sprites = plugin.scene.data.get('sprites')
		plugin.players = plugin.scene.data.get('players')
		plugin.world = plugin.newWorld()
		// setup some generic proxies for ease of use
		plugin.world.p = plugin.createProxies()

		plugin.paused = false

		// Start the game tick system
		plugin.startTick()
	}

	/**
	 * @description called each time the scene is initialized.
	 */
	init(data) {
		//console.debug('GameManagerPlugin.init(data)', data)
		plugin.state.data = data
	}

	/**
	 * @description Starts the game tick system.
	 */
	startTick() {
		plugin.changeTick()
	}

	changeTick(newTick) {
		// Clear any existing tick interval
		if (plugin.tickInterval) {
			plugin.tickInterval.remove(true) // false ?!
		}
		if (newTick) {
			plugin.state.simTickTimePrev = plugin.state.simTickTime
			plugin.state.simTickTime = newTick
		}
		// Reschedule the next tick
		plugin.tickInterval = plugin.scene.time.addEvent({
			delay: plugin.state.simTickTime,
			callback: plugin.nextTick,
			callbackScope: this,
			loop: true
		})
	}

	/**
	 * @description Advances the game tick, processes queues, and emits gameTick event.
	 */
	nextTick() {
		if (plugin.paused) {
			scene.updatePause()
			return
		}

		// Increment simulation tick
		plugin.state.tick += 1
		if (plugin.state.tick > MAX_INT32) {
			plugin.state.tick = 0 // Reset tick to 0 if it exceeds the maximum 32-bit integer value
		}
		plugin.scene.events.emit(`gameTick:${plugin.state.tick}`)

		// Increment simulation time
		plugin.state.simElapsedTime += plugin.state.simTickTime

		// Emit a `gameTick:sec` event every second
		this.secondsElapsed += plugin.state.simTickTime
		if (this.secondsElapsed >= 1000) {
			plugin.scene.events.emit('gameTick:sec')
			this.secondsElapsed -= 1000
		}

		// Process and render based on processTickTime
		plugin.state.processElapsedTime += plugin.state.simTickTime

		// Check if enough time has passed to perform processing
		if (plugin.state.processElapsedTime >= plugin.state.processTickTime) {
			plugin.state.processElapsedTime = 0 // Reset processing elapsed time
			plugin.moveToProcessQ()
			plugin.process()

		}
		if (plugin.state.tick > 2) {
			plugin.render()
		}
	}

	/**
	 * @description Moves items from recvQ to processQ.
	 */
	moveToProcessQ() {
		plugin.state.processQ = plugin.state.recvQ.slice()
		plugin.state.recvQ = []
	}

	/**
	 * @description Processes items in processQ and moves them to renderQ.
	 */
	process() {
		plugin.state.renderQ = plugin.state.processQ.slice()
		plugin.state.processQ = []
		plugin.processed()
	}

	/**
	 * @description Placeholder method for additional processing logic.
	 */
	processed() {
		// Placeholder for additional processing logic
		if (!plugin.scene.mq) {
			return
		}
		if (plugin.scene.gameData.isHost) {
			plugin.scene.gm.sendWorldTick(plugin.state.tick, plugin.state.simTickTime)
			//console.debug('sendWorldTick', plugin.state.tick, plugin.state.simTickTime)
		}

	}

	/**
	 * @description Placeholder method for render or if exist call scene render method.
	 */
	render() {
		if (plugin.scene.render) {
			plugin.scene.render()
		}
	}

	/**
	 * @description Receives an object and pushes it to recvQ if tick matches. Adds a received timestamp.
	 * @param {Object} obj - The object to receive.
	 * @returns {Object} The received object with a timestamp or received=false.
	 */
	recv(obj) {
		if (obj.tick === plugin.state.tick) {
			obj.received = Date.now()
			plugin.state.recvQ.push(obj)
			return obj
		} else {
			obj.received = false
			return obj
		}
	}

	/**
	 * @description Pauses or resumes the tick system.
	 * @param {boolean} pause - True to pause, false to resume.
	 * @param {number} pauseDelaySwitch - Delay in milliseconds before switching to the previous state.
	 */
	pause(pause, pauseDelaySwitch = false) {
		plugin.paused = pause
		//plugin.tickInterval.paused = pause
		if (pauseDelaySwitch) {
			setTimeout(() => {
				plugin.paused = !pause
			}, pauseDelaySwitch)
		}

	}

	/**
	 * @description Converts a time in milliseconds to the equivalent number of ticks.
	 * @param {number} timeMs - Time in milliseconds.
	 * @returns {number} The equivalent number of ticks.
	 */
	timeToTick(timeMs, addCurrentTick = true) {
		let result = Math.ceil(timeMs / plugin.state.simTickTime)
		if (addCurrentTick) {
			result += plugin.state.tick
		}
		return result
	}

	/**
	 * @description Sets a callback to be called at a specific tick.
	 * @param {number} tick - The tick at which to call the callback.
	 * @param {Function} cb - The callback function.
	 */
	onTick(tick, cb) {
		plugin.scene.events.once(`gameTick:${tick}`, cb, this)
	}

	/**
	 * @description Saves the DataManager state to JSON.
	 * @returns {string} The serialized state as a JSON string.
	 */
	save() {
		const state = plugin.scene.data.getAll()
		return JSON.stringify(state)
	}

	/**
	 * @description Loads the DataManager state from JSON.
	 * @param {string} json - The serialized state as a JSON string.
	 */
	load(json) {
		const state = JSON.parse(json)
		plugin.scene.data.setAll(state)
		plugin.state = plugin.scene.data.get('state')
		plugin.sprites = plugin.scene.data.get('sprites')
		plugin.players = plugin.scene.data.get('players')
	}
}

// Extended plugin class with mixins applied
class GameManagerPlugin extends BaseGameManagerPlugin { }
applyMixins(GameManagerPlugin, [GamePlayerMixin, GameEntityMixin, GameWorldMixin])

export { GameManagerPlugin }