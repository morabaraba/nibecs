/** global Phaser */
import {
	createWorld,
	addEntity,
	addComponent,
	defineSerializer,
	defineDeserializer,
	DESERIALIZE_MODE
} from 'thirdparty/bitecs'
import { InputProxy, PositionProxy, VelocityProxy, SpriteProxy } from 'proxies'
import { Position, Velocity, Rotation } from 'components/movement'
import { Sprite } from 'components/entity'

/** @class */
class EcsPlugin extends Phaser.Plugins.BasePlugin {
	constructor(pluginManager) {
		super('EcsPlugin', pluginManager);
		this.data = null
	}
	init(data) {
		//console.log('EcsPlugin init.');
		this.data = data
	}
	newWorld() {
		// create world
		let w = createWorld()
		// setup proxies
		w.input = new InputProxy(0)
		w.position = new PositionProxy(0)
		w.sprite = new SpriteProxy(0)
		w.newEntity = (components) => {
			let eid = addEntity(w)
			if (!components) { // defaults
				addComponent(w, Position, eid)
				addComponent(w, Velocity, eid)
				addComponent(w, Rotation, eid)
				addComponent(w, Sprite, eid)
			} else {
				components.array.forEach(comp => {
					addComponent(w, comp, eid)
				});
			}
			return eid
		}
		return w
	}

	updateGame2() {
		let game1 = this.data.game1
		let game2 = this.data.game2
		let scene1 = game1.scene.scenes[0]
		let scene2 = game2.scene.scenes[0]
		let packet = scene1.serializeWorld()
		let deserializedEnts = scene2.deserializeWorld(packet)
		//console.log('updateGame2', deserializedEnts)
	}

}

export { EcsPlugin }