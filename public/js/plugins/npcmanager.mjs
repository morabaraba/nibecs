/**
 * @class NpcManagerPlugin
 * @extends Phaser.Plugins.ScenePlugin
 * @description Plugin to manage NPCs using GameManagerPlugin.
 */
class NpcManagerPlugin extends Phaser.Plugins.ScenePlugin {
    boot() {
        // Initialize DataManager items
        this.scene.data.set('npcs', {});

        this.npcs = this.scene.data.get('npcs');
        this.spriteSheetName = '' // TODO move to init with data to configure
        this.spriteSheet = {} // TODO above

        // Hook the update method to the scene
        this.scene.events.on('update', this.update, this);
    }

    /**
     * @description Adds an NPC to the game.
     * @param {string} npcType - The type of the NPC.
     * @param {Object} npcData - The data associated with the NPC.
     * @param {Function} npcFunc - The function to execute for the NPC each tick.
     * @returns {number} The index of the added NPC.
     */
    addNpc(npcType, npcData, npcFunc) {
        const entity_idx = this.scene.gameManagerPlugin.addEntity(this.createNpcSprite(npcType));
        this.npcs[entity_idx] = {
            npcType,
            npcData,
            npcFunc,
            npcAlive: true
        };
        return entity_idx;
    }

    /**
     * @description Destroys an NPC in the game.
     * @param {number} entity_idx - The index of the NPC to destroy.
     */
    destroyNpc(entity_idx) {
        if (this.npcs[entity_idx]) {
            this.scene.gameManagerPlugin.destroyEntity(entity_idx);
            delete this.npcs[entity_idx];
        }
    }

    /**
     * @description Creates a sprite for the NPC based on its type.
     * @param {string} npcType - The type of the NPC.
     * @returns {Phaser.GameObjects.Sprite} The created sprite.
     */
    createNpcSprite(npcType) {
        // Replace with actual sprite creation logic
        const x = Phaser.Math.Between(0, 800);
        const y = Phaser.Math.Between(0, 600);
        return this.scene.add.sprite(x, y, this.spriteSheetName, this.spriteSheet[npcType]);
    }

    /**
     * @description Updates the NPCs each tick.
     */
    update() {
        for (const entity_idx in this.npcs) {
            const npc = this.npcs[entity_idx];
            if (npc.npcAlive) {
                npc.npcAlive = npc.npcFunc(entity_idx, npc.npcType, npc.npcData, this);
            }
        }
    }

    /**
     * @description Updates the behavior of a zombie NPC.
     * @param {number} entity_idx - The index of the NPC entity.
     * @param {string} npcType - The type of the NPC.
     * @param {Object} npcData - The data associated with the NPC.
     * @param {NpcManagerPlugin} thisPlugin - The instance of the NpcManagerPlugin.
     * @returns {boolean} Whether the NPC is still alive.
     */
    updateZombie(entity_idx, npcType, npcData, thisPlugin) {
        const npcSprite = thisPlugin.scene.gameManagerPlugin.entities[entity_idx];
        if (!npcSprite) return false;

        const players = Object.values(thisPlugin.scene.gameManagerPlugin.players);
        let target = null;
        let minDistance = Infinity;

        // Find the closest player
        players.forEach(player => {
            const playerSprite = thisPlugin.scene.gameManagerPlugin.entities[player.entity];
            if (playerSprite) {
                const distance = Phaser.Math.Distance.Between(npcSprite.x, npcSprite.y, playerSprite.x, playerSprite.y);
                if (distance < minDistance) {
                    minDistance = distance;
                    target = playerSprite;
                }
            }
        });

        // If no player found, find the closest entity
        if (!target) {
            thisPlugin.scene.gameManagerPlugin.entities.forEach(entity => {
                if (entity && entity !== npcSprite) {
                    const distance = Phaser.Math.Distance.Between(npcSprite.x, npcSprite.y, entity.x, entity.y);
                    if (distance < minDistance) {
                        minDistance = distance;
                        target = entity;
                    }
                }
            });
        }

        // Move towards the
    }
}

export { NpcManagerPlugin }