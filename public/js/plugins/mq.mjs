/** global Phaser */
//import { Paho } from 'paho-mqtt';
import { generateUUID, stringToUint8Array, simpleHash } from 'utils'

let plugin; // forward decleration for getting rid of this in the plugin, so just the in a single class!

/**
 * MqPlugin handles the Message Queue (MQ) system using Paho-MQTT.
 */
class MqPlugin extends Phaser.Plugins.BasePlugin {
	constructor(pluginManager) {
		super('MqPlugin', pluginManager)
		plugin = this // on construct setup forward
		plugin.data = null
		plugin.client = null
	}

	/**
	 * Initializes the plugin with data.
	 * @param {Object} data - Initialization data.
	 */
	init(data) {
		//console.log('MQ Plugin init.');
		plugin.data = data
		plugin.data.gameType = data.settings['game-type']
		plugin.data.gameName = data.settings['game-name']
		plugin.data.gamePass = data.settings['game-pass']
		plugin.data.isHost = plugin.data.gameType == 'host'
		plugin.isHost = plugin.data.isHost // Make it even easier to access directly on the plugin
		if (plugin.isHost) {
			plugin.injectJoinButton()
		}
		const clientId = 'planeteer-mooo-kom-' + data.settings['player-1-nick'] + '-' + generateUUID()[2]
		plugin.data.mqttSettings = {
			hostname: '178.79.178.70',
			port: 15675,
			path: '/ws',
			connectOptions: {
				useSSL: false, // Ensure it's actually secure in a real scenario
				userName: 'rugraat', // Consider secure storage and handling for credentials
				password: 'rugraat',
				keepAliveInterval: 30 // Ping the server every 30 seconds to keep the connection alive
			},
			clientId: clientId,
			topic: '🕹️',
		}
		plugin.connectMq()
	}

	injectJoinButton() {
		// Add fullscreen button
		const button = document.createElement('a');
		button.classList.add('btn');
		button.id = 'joinButton';
		button.innerHTML = '&nbsp;👥<br>Join<br>Link';
		document.body.appendChild(button);

		// Add onclick functionality
		button.onclick = () => {
			// Get the current window URL
			let currentUrl = window.location.href;

			// Change game-type=host to game-type=join
			let newUrl = currentUrl.replace('game-type=host', 'game-type=join');

			function fallBackMethod(err) {
				console.error('Failed to copy: ', err);
				alert('Could not copy url to clipboard, opening new window copy window url to other player.');

				// Fallback to open a pop-up window with the URL
				window.open(newUrl, '_blank');
			}
			if (!navigator.clipboard) {
				fallBackMethod('navigator.clipboard is undefined')
				return
			}
			// Try to copy the new URL to the clipboard
			navigator.clipboard.writeText(newUrl).then(() => {
				console.debug('Join link copied to clipboard!');
				alert('Join link copied to clipboard!');
			}).catch(err => {
				fallBackMethod(err)
			});
		};
	}

	/**
	 * Connects to the MQTT broker.
	 */
	connectMq() {
		plugin.hostTopic = false
		const gameHash = simpleHash(plugin.data.gameName).slice(4)
		const gameHashPass = simpleHash(plugin.data.gamePass).slice(4)
		if (plugin.data.isHost) {
			plugin.hostTopic = plugin.data.mqttSettings.topic + '/' + gameHash + '/' + gameHashPass
			plugin.joinTopic = plugin.data.mqttSettings.topic + '/' + gameHash + '/t/' + gameHashPass
		} else {
			plugin.joinTopic = plugin.data.mqttSettings.topic + '/' + gameHash + '/t/' + gameHashPass
		}
		let mqttSettings = plugin.data.mqttSettings
		// Create a new MQTT client instance
		const client = plugin.client = new Paho.Client(
			mqttSettings.hostname,
			mqttSettings.port,
			mqttSettings.path,
			mqttSettings.clientId
		)
		// Set callback handlers
		client.onConnectionLost = plugin.onConnectionLost.bind(this)
		client.onMessageArrived = plugin.onMessageArrived.bind(this, client)

		// Connect to the MQTT broker
		client.connect({
			...mqttSettings.connectOptions,
			onSuccess: plugin.onConnect.bind(this, client),
			onFailure: plugin.onFailure.bind(this)
		})
	}

	/**
	 * Handles connection loss.
	 * @param {Object} responseObject - The response object containing error information.
	 */
	onConnectionLost(responseObject) {
		if (responseObject.errorCode !== 0) {
			//console.log("Connection lost: " + responseObject.errorMessage);
		}
	}

	/**
	 * Handles messages arriving from the MQTT broker.
	 * @param {Object} client - The MQTT client instance.
	 * @param {Object} message - The arrived message.
	 */
	onMessageArrived(client, message) {
		if (message.destinationName == plugin.joinTopic) {
			const data = plugin.decodeMessage(message)
			//console.debug("[" + data.latency + " ms] Host Message Arrived: " + message.destinationName + " - " + JSON.stringify(data));
			if (plugin.hostMessageArrive) {
				plugin.hostMessageArrive(data)
			}
		} else if (message.destinationName.includes(plugin.hostTopic)) {
			const data = plugin.decodeMessage(message)
			console.debug("[" + data.latency + " ms] Client Message Arrived: " + message.destinationName + " - " + JSON.stringify(data));
		} else {
			//console.debug("Unknown Message Arrived: " + message.destinationName + " - " + message.payloadBytes);
		}
	}

	/**
	 * Handles successful connection to the MQTT broker.
	 * @param {Object} client - The MQTT client instance.
	 */
	onConnect(client) {
		//console.log("Connected to MQTT broker");
		plugin.data.isConnected = true // Set isConnected to true
		if (plugin.hostTopic) {
			// Subscribe to the host topic
			//console.log("Creating Host");
			client.subscribe(plugin.hostTopic, {
				onSuccess: function () {
					//console.log("Subscribed to topic:" + plugin.hostTopic);
				}.bind(this),
				onFailure: function (error) {
					//console.log("Failed to subscribe: " + error.errorMessage);
				}
			})
		}
		//console.log("Creating Client");
		client.subscribe(plugin.joinTopic, {
			onSuccess: function () {
				//console.log("Subscribed to topic:" + plugin.joinTopic);
			}.bind(this),
			onFailure: function (error) {
				//console.log("Failed to subscribe: " + error.errorMessage);
			}
		})
	}

	/**
	 * Handles failure to connect to the MQTT broker.
	 * @param {Object} error - The error object containing error information.
	 */
	onFailure(error) {
		//console.log("Failed to connect: " + error.errorMessage);
		plugin.data.isConnected = false // Set isConnected to false
		alert("Failed to connect: " + error.errorMessage) // Alert the error
	}

	/**
	 * Encodes a message to be sent.
	 * @param {Array} prefix - The prefix data.
	 * @param {Uint8Array} packet - The packet data.
	 * @returns {Paho.Message} - The encoded message.
	 */
	encodeMessage(prefix, packet) {
		// Convert the prefix parts to Uint8Array
		const typeArray = stringToUint8Array(prefix[0]) // 3-char string to Uint8Array

		// Convert timestamp to BigInt and then to byte arrays
		const timeStampBigInt = BigInt(prefix[1])
		const timeStampArray = new ArrayBuffer(8) // BigInt will take 8 bytes
		new DataView(timeStampArray).setBigUint64(0, timeStampBigInt, true) // Use little-endian

		// Convert tick to byte arrays with specified endianness
		const tickArray = new ArrayBuffer(4)
		new DataView(tickArray).setInt32(0, prefix[2], true) // Use little-endian

		// Combine all parts into a single Uint8Array
		const combinedArray = new Uint8Array(
			typeArray.byteLength + timeStampArray.byteLength + tickArray.byteLength + packet.byteLength
		)
		combinedArray.set(new Uint8Array(typeArray), 0)
		combinedArray.set(new Uint8Array(timeStampArray), typeArray.byteLength)
		combinedArray.set(new Uint8Array(tickArray), typeArray.byteLength + timeStampArray.byteLength)
		combinedArray.set(packet, typeArray.byteLength + timeStampArray.byteLength + tickArray.byteLength)

		return new Paho.Message(combinedArray)
	}

	/**
	 * Decodes a received message.
	 * @param {Object} msg - The received message.
	 * @returns {Object} - The decoded message data.
	 */
	decodeMessage(msg) {
		const pongTime = new Date().getTime()
		const decodedArray = new Uint8Array(msg.payloadBytes)

		// Extract the prefix parts
		const typeArray = decodedArray.slice(0, 3)
		const type = new TextDecoder().decode(typeArray)
		const timeStampArray = decodedArray.slice(3, 11)
		const ts = new DataView(timeStampArray.buffer).getBigUint64(0, true) // Use little-endian
		const tickArray = decodedArray.slice(11, 15)
		const tick = new DataView(tickArray.buffer).getInt32(0, true) // Use little-endian
		const latency = pongTime - Number(ts) // Convert BigInt to Number
		// Extract the packet
		const packet = decodedArray.slice(15)

		// Return the prefix and packet as an object
		return {
			type: type,
			timestamp: Number(ts), // Convert BigInt to Number
			latency: latency,
			tick: tick,
			packet: packet
		}
	}
}

export { MqPlugin }
