/** global Phaser */

/**
 * @class MusicPlugin
 * @extends Phaser.Plugins.ScenePlugin
 * @description A plugin to handle music playback and display credits information using either the Web Audio API or Phaser Audio API.
 */
class MusicPlugin extends Phaser.Plugins.ScenePlugin {
	/**
	 * @param {Phaser.Scene} scene - The scene that the plugin is bound to.
	 * @param {Phaser.Plugins.PluginManager} pluginManager - The plugin manager.
	 * @param {boolean} usePhaserAudio - Whether to use Phaser's audio system. Defaults to true.
	 */
	constructor(scene, pluginManager, usePhaserAudio = true) {
		super(scene, pluginManager)
		this.usePhaserAudio = false //usePhaserAudio;
		this.audioContext = null
		this.gainNode = null
		this.currentTrack = null
		this.musicCredits = []
		this.previousTrackIndex = -1
		this.musicPath = 'assets/music/song2'
		this.isMuted = true
		this.animationInterval = null
		this.grow = true

		if (!this.usePhaserAudio) {
			this.audioContext = new (window.AudioContext || window.webkitAudioContext)()
			this.gainNode = this.audioContext.createGain()
			this.gainNode.connect(this.audioContext.destination)
		}

		// Ensure music toggle button exists
		const musicButton = document.getElementById('musicButton') || document.createElement('a')
		if (!document.getElementById('musicButton')) {
			musicButton.classList.add('btn')
			musicButton.id = 'musicButton'
			musicButton.textContent = '🔇'
			document.body.appendChild(musicButton)
		}
		this.musicButton = musicButton
		this.setupButton()
		this.setupVisibilityHandler()
	}

	/**
	 * Start function to initiate the music plugin.
	 */
	start() {
		this.loadCredits()
	}

	/**
	 * Loads the credits.json file and audio assets.
	 */
	loadCredits() {
		if (this.usePhaserAudio) {
			this.scene.load.json('credits', `${this.musicPath}/credits.json`)
			this.scene.load.once('complete', () => {
				this.musicCredits = this.scene.cache.json.get('credits')
				this.musicCredits.forEach(credit => {
					this.scene.load.audio(credit.title, `${this.musicPath}/${credit.mp3}`)
					this.scene.load.image(credit.title + '_image', `${this.musicPath}/${credit.image}`)
				})

				this.scene.load.once('complete', () => {
					this.playInitialTrack()
				})

				this.scene.load.start()
			})
			this.scene.load.start()
		} else {
			this.fetchCredits()
		}
	}

	/**
	 * Fetches music credits from a JSON file.
	 */
	async fetchCredits() {
		try {
			const response = await fetch(`${this.musicPath}/credits.json`)
			this.musicCredits = await response.json()
			this.playRandomTrack()
		} catch (error) {
			console.error('Error fetching credits:', error)
		}
	}

	/**
	 * Plays the initial track based on URL parameters.
	 */
	playInitialTrack() {
		const gameMusic = this.scene.gameData.settings['game-music']
		const gameMusicTime = parseFloat(this.scene.gameData.settings['game-music-time']) || 0
		if (gameMusic) {
			const credit = this.musicCredits.find(credit => credit.mp3 === gameMusic)
			if (credit) {
				this.playTrack(credit, gameMusicTime)
				this.showCredit(credit)
			} else {
				this.playRandomTrack()
			}
		} else {
			this.playRandomTrack()
		}
	}

	/**
	 * Plays a specific track and starts it at a specific time.
	 *
	 * @param {Object} credit - The music credit object.
	 * @param {number} startTime - The start time in seconds.
	 */
	playTrack(credit, startTime = 0) {
		if (this.usePhaserAudio) {
			if (this.currentTrack) {
				this.currentTrack.stop()
			}
			const audio = this.scene.sound.add(credit.title)
			audio.play()
			audio.setSeek(Math.floor(startTime * 1000)) // TODO why not working?
			this.currentTrack = audio
			this.showCredit(credit)
			audio.once('complete', this.playRandomTrack, this)
		} else {
			this.playWebAudioTrack(credit, startTime)
		}
	}

	/**
	 * Plays a specific track using Web Audio API and starts it at a specific time.
	 *
	 * @param {Object} credit - The music credit object.
	 * @param {number} startTime - The start time in seconds.
	 */
	async playWebAudioTrack(credit, startTime = 0) {
		if (this.currentTrack) {
			this.currentTrack.stop()
		}
		const audioBuffer = await this.fetchAndDecodeAudio(credit.mp3)
		this.currentTrack = this.audioContext.createBufferSource()
		this.currentTrack.buffer = audioBuffer
		this.currentTrack.connect(this.gainNode) // Connect to gain node
		this.currentTrack.start(0, startTime)
		this.showCredit(credit)
		this.currentTrack.onended = this.playRandomTrack.bind(this)
	}

	/**
	 * Plays a random track from the credits.
	 */
	playRandomTrack() {
		let randomIndex
		do {
			randomIndex = Math.floor(Math.random() * this.musicCredits.length)
		} while (randomIndex === this.previousTrackIndex)

		this.previousTrackIndex = randomIndex
		const credit = this.musicCredits[randomIndex]
		this.playTrack(credit)
	}

	/**
	 * Fetches and decodes audio using Web Audio API.
	 *
	 * @param {string} url - The URL of the audio file.
	 * @returns {Promise<AudioBuffer>} - The decoded audio buffer.
	 */
	async fetchAndDecodeAudio(url) {
		const response = await fetch(`${this.musicPath}/${url}`)
		const arrayBuffer = await response.arrayBuffer()
		return this.audioContext.decodeAudioData(arrayBuffer)
	}

	/**
	 * Displays the music credit information on screen and fades it out.
	 *
	 * @param {Object} credit - The music credit object.
	 */
	showCredit(credit) {
		if (this.creditContainer) {
			return // double load glitch hack to fix TODO
		}
		const titles = credit.title.split('-')
		const creditContainer = this.creditContainer = this.scene.add.container(this.scene.sys.canvas.width - 200, this.scene.sys.canvas.height - 60)

		// Create the credit text and center align it
		const creditText = this.scene.add.text(0, 0, `${titles[0]}\n${titles[1]}`, { fontSize: '16px', fill: '#fff', align: 'center' })

		// Calculate the width of the text and center it
		creditText.setOrigin(0.5, 0.5)

		// Create the credit image
		const creditImage = this.scene.add.image(-0, 0, credit.title + '_image').setDisplaySize(64, 64).setOrigin(1, 0.5)

		// Add padding between the image and text
		const padding = -32
		creditImage.x = creditText.x + creditText.displayWidth + padding

		// Add elements to the container
		creditContainer.add([creditText, creditImage])

		// Tween the container to fade out
		this.scene.tweens.add({
			targets: creditContainer,
			alpha: 0,
			duration: 4000,
			ease: 'Power1',
			onComplete: () => {
				this.creditContainer.destroy()
				this.creditContainer = null
			}
		})
	}

	/**
	 * Sets up the music button and its event listener.
	 */
	setupButton() {
		this.musicButton.style.display = 'block' // Make the button visible
		this.musicButton.addEventListener('click', () => this.toggleButton(!this.isMuted))
		this.toggleButton(this.isMuted)
	}

	/**
	 * Toggles the mute state and updates the button display.
	 *
	 * @param {boolean} mute - Whether to mute the audio.
	 */
	toggleButton(mute) {
		if (this.audioContext && this.audioContext.state === 'suspended') {
			this.audioContext.resume()
			if (this.audioContext.state === 'suspended') {
				mute = true
			}
		}
		if (this.usePhaserAudio) {
			if (this.currentTrack) {
				this.currentTrack.setMute(mute)
			}
		} else {
			this.gainNode.gain.value = mute ? 0 : 1
		}
		this.musicButton.textContent = mute ? '🔇' : '🔊'
		this.tweenButton(mute)
		this.isMuted = mute
	}

	/**
	 * Animates the music button based on mute state.
	 *
	 * @param {boolean} isMuted - Whether the audio is muted.
	 */
	tweenButton(isMuted) {
		if (this.animationInterval) {
			clearInterval(this.animationInterval)
		}
		if (isMuted) {
			// Start the grow/shrink animation
			this.animationInterval = setInterval(() => {
				this.musicButton.style.transform = this.grow ? 'scale(1.1)' : 'scale(1)'
				this.grow = !this.grow
			}, 500)
		} else {
			// Stop the grow/shrink animation
			clearInterval(this.animationInterval)
			this.musicButton.style.transform = 'scale(1)'
		}
	}

	/**
	 * Sets up the visibility change handler to resume audio context.
	 */
	setupVisibilityHandler() {
		document.addEventListener('visibilitychange', () => {
			if (document.visibilityState === 'visible') {
				if (this.usePhaserAudio) {
					if (this.scene.sound.context.state === 'suspended') {
						this.scene.sound.context.resume()
					}
				} else {
					if (this.audioContext.state === 'suspended') {
						this.audioContext.resume()
					}
				}
			}
		})
	}

	/**
	 * Stops the current track.
	 */
	stopTrack() {
		if (this.currentTrack) {
			this.currentTrack.stop()
			this.currentTrack = null
		}
	}

	/**
	 * Seeks the current track to a specific time.
	 * @param {number} time - The time to seek to in seconds.
	 */
	seekTrack(time) {
		if (this.usePhaserAudio && this.currentTrack) {
			this.currentTrack.setSeek(time)
		} else if (this.currentTrack) {
			this.currentTrack.stop()
			this.playWebAudioTrack(this.musicCredits[this.previousTrackIndex], time)
		}
	}

	/**
	 * Mutes or unmutes the audio.
	 * @param {boolean} mute - Whether to mute the audio.
	 */
	mute(mute) {
		this.toggleButton(mute)
	}
}

// Export the MusicPlugin class
export { MusicPlugin }
