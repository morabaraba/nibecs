/**
 * Creates an enum-like object from an array of values.
 *
 * @param {Array} values - The array of values to create the enum from.
 * @returns {Object} The enum-like object with the values as keys and values.
 */
function createEnum(values) {
	const enumObject = {}
	for (const val of values) {
		enumObject[val] = val
	}
	return Object.freeze(enumObject) // Freeze the object to prevent modifications
}

/**
 * Generates a random UUID (Universally Unique Identifier).
 *
 * @returns {string} A random 16-digit UUID string.
 */
function generateUUID() {
	// Generate a random 16-digit UUID string
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8)
		return v.toString(16)
	})
}

/**
 * Applies multiple mixins to a target class.
 * @param {Function} targetClass - The class to which the mixins will be applied.
 * @param {Function[]} mixins - An array of mixin constructors to apply.
 * @description On the mixin a constructorMixin(...) can be declared to be called by
 * this.superMixins(arguments) inside the Base Class constructor.
 */
function applyMixins(targetClass, mixins) {
	// Create an array to store the mixin constructors
	targetClass.prototype._mixins = []

	mixins.forEach((mixin, idx) => {
		// Copy methods excluding constructor
		Object.getOwnPropertyNames(mixin.prototype).forEach(name => {
			if (name !== 'constructor') {
				targetClass.prototype[name] = mixin.prototype[name]
			}
		})

		// Hook mixin constructors
		const constructorMixin = function (args) {
			if (mixin.prototype.constructorMixin) {
				mixin.prototype.constructorMixin.apply(this, args)
			} else {
				console.debug('No constructMixin', targetClass, mixin, idx)
			}
		}

		// Store the mixin constructor in the _mixins array
		targetClass.prototype._mixins.push({
			constructorMixin
		})
	})

	/**
	 * Calls the constructors of all mixins.
	 * @param {...*} args - The arguments to pass to each mixin constructor.
	 */
	targetClass.prototype.superMixins = function (...args) {
		this._mixins.forEach((mixin, idx) => {
			mixin.constructorMixin.apply(this, args)
		})
	}
}

/**
 * Utility function to convert a string to Uint8Array.
 * @param {string} str - The string to convert.
 * @returns {Uint8Array} - The converted Uint8Array.
 */
function stringToUint8Array(str) {
	const utf8Encoder = new TextEncoder()
	return utf8Encoder.encode(str)
}

/**
 * Generates a simple hash for a given string and converts it to a hexadecimal string.
 *
 * @param {string} str - The input string to hash.
 * @returns {string} - The hexadecimal representation of the hash value.
 */
function simpleHash(str) {
	let hash = 0

	// Loop through each character in the string
	for (let i = 0; i < str.length; i++) {
		// Get the ASCII code of the character
		const char = str.charCodeAt(i)

		// Update the hash value using bitwise operations
		hash = (hash << 5) - hash + char
		hash |= 0 // Convert to a 32-bit integer
	}

	// Convert the final hash value to a hexadecimal string
	return hash.toString(16)
}

function hexColorStringToInt(hexString) {
	// Remove the hash (#) from the beginning of the string
	hexString = hexString.replace('#', '')
	// Convert the hexadecimal string to an integer
	return parseInt(hexString, 16)
}

export { createEnum, generateUUID, applyMixins, stringToUint8Array, simpleHash, hexColorStringToInt }