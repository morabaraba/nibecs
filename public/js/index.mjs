//import { setupGame as loadGameSync } from 'game'
import { setupGame as loadGameSolar } from 'solar'
import { setupGame as loadGameSports } from 'sports'

// Game Selection
const urlParams = new URLSearchParams(window.location.search);
const gameParam = urlParams.get('game');
if (gameParam === 'sync') {
    window.game = loadGameSync();
} else if (gameParam === 'solar') {
    window.game = loadGameSolar();
} else if (gameParam === 'sports') {
    window.game = loadGameSports();
} else {
    console.error('No Game Selected');
    window.location = 'sports.html'
}
