import { createWorld, addEntity, addComponent,  } from 'thirdparty/bitecs'

import { Position, Velocity } from 'components/movement'
import { movementSystem } from 'systems/movement'

QUnit.test('movementSystem updates position correctly', function (assert) {
  const world = createWorld();
  world.time = { delta: 16, elapsed: 0, then: performance.now() };

  const eid = addEntity(world);
  addComponent(world, Position, eid);
  addComponent(world, Velocity, eid);
  Velocity.x[eid] = 1.23;
  Velocity.y[eid] = 1.23;

  movementSystem(world);

  // Check if position was updated correctly
  assert.ok(Math.abs(Position.x[eid] - 19.68) < 0.01, 'Position.x is updated correctly');
  assert.ok(Math.abs(Position.y[eid] - 19.68) < 0.01, 'Position.y is updated correctly');
  assert.notOk(Math.abs(Position.z[eid] - 19.68) < 0.01, 'Position.z  correctly'); // shoud fail
});
