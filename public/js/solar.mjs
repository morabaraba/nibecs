/** global Phaser */
import { SolarSystemScene } from 'scenes/solar'
import { EcsPlugin } from 'plugins/ecs'


function setupGame() {


	const gameData = { }
	const ZOOM_LEVEL = 1
	/** @type Phaser.Types.Core.GameConfig */
	const config = {
		type: Phaser.AUTO,
		mode: Phaser.Scale.NONE,
		width: window.innerWidth/ZOOM_LEVEL,
		height: window.innerHeight/ZOOM_LEVEL,
		zoom: ZOOM_LEVEL,
		physics: {
			default: 'arcade',
			arcade: {
				gravity: { y: 200 }
			}
		},
		scene: [SolarSystemScene],
		disableContextMenu: true,
		plugins: {
			global: [
				{ key: 'EcsPlugin', plugin: EcsPlugin, start: true, data: gameData },

			]
		}
	}

	let game = new Phaser.Game(config)

	window.addEventListener("resize", () => {
			game.scale.resize(window.innerWidth/ZOOM_LEVEL, window.innerHeight/ZOOM_LEVEL);
		},false
	);

	return game
}


export { setupGame }