
import {createKeyboardMapping, createActionMapping, createGamepadMapping, createMouseMapping } from 'wrappers/keys'

/**
 * @class GamePlayerMixin
 */
export default class GamePlayerMixin {

	/**
	 * @description Adds a player to the game.
	 * @param {string} uuid - The unique identifier for the player.
	 * @param {Phaser.GameObjects.Sprite} sprite - The sprite representing the player.
	 */
	addPlayer(sprite) {
		//uuid, sprite, components
		const state = sprite.state || {}
		const data = sprite.data.get('data') || {}
		const eid = sprite.eid =
			state.eid = data.eid = this.addEntity(sprite, sprite.state.components)
		sprite.state.data = data
		sprite.state = state
		sprite.data.set('data', data)
		this.players[eid] = sprite
		return eid
	}

	/**
	 * @description Destroys a player in the game.
	 * @param {number} eid - The unique eid for the player.
	 */
	destroyPlayer(eid) {
		const sprite = this.players[eid]
		if (sprite) {
			// destroy in ecs world
			this.destroyEntity(eid)
			// delete object
			delete this.players[eid]
			// destroy phaser sprite
			sprite.destroy()
		}
	}

	updatePlayers() {
		function updateKeys(playerId, playerData) {
			const keys = playerData.keys
			const eid = playerData.eid
			if (!keys) return
			let input = world.input
			input.eid = eid
			input.directionX = 0
			input.directionY = 0
			if (keys.left.isDown) input.directionX += -1
			if (keys.right.isDown) input.directionX += 1
			if (keys.up.isDown) input.directionY += -1
			if (keys.down.isDown) input.directionY += 1
		}
	}

	selectKeys2PlayerKeys(option, scene) {
		option = option.replace(/\u2003/g, ' ').replace(/ +/g, ' ').trim();
		console.log('selectKeys2PlayerKeys', option);  // Output: "| ↑←↓→ | , | . | /"

		let parts = option.split('|').map(part => part.trim())

		const keyCodeMap = {
			'ins': Phaser.Input.Keyboard.KeyCodes.INSERT,
			'del': Phaser.Input.Keyboard.KeyCodes.DELETE,
			'home': Phaser.Input.Keyboard.KeyCodes.HOME,
			'end': Phaser.Input.Keyboard.KeyCodes.END,
			'pgup': Phaser.Input.Keyboard.KeyCodes.PAGE_UP,
			'pgdown': Phaser.Input.Keyboard.KeyCodes.PAGE_DOWN,
			'q': Phaser.Input.Keyboard.KeyCodes.Q,
			'w': Phaser.Input.Keyboard.KeyCodes.W,
			'e': Phaser.Input.Keyboard.KeyCodes.E,
			'1': Phaser.Input.Keyboard.KeyCodes.ONE,
			'2': Phaser.Input.Keyboard.KeyCodes.TWO,
			'3': Phaser.Input.Keyboard.KeyCodes.THREE,
			'7': Phaser.Input.Keyboard.KeyCodes.SEVEN,
			'8': Phaser.Input.Keyboard.KeyCodes.EIGHT,
			'9': Phaser.Input.Keyboard.KeyCodes.NINE,
			'num7': Phaser.Input.Keyboard.KeyCodes.NUMPAD_SEVEN,
			'num8': Phaser.Input.Keyboard.KeyCodes.NUMPAD_EIGHT,
			'num9': Phaser.Input.Keyboard.KeyCodes.NUMPAD_NINE,
			'enter': Phaser.Input.Keyboard.KeyCodes.ENTER,
			',': Phaser.Input.Keyboard.KeyCodes.COMMA,
			'.': Phaser.Input.Keyboard.KeyCodes.PERIOD,
			'/': Phaser.Input.Keyboard.KeyCodes.FORWARD_SLASH
		}

		switch (parts[1]) {
			case 'network':
			case 'npc':
				return parts[1]
			case 'disabled':
				return null
			case '↑←↓→':
			case 'wasd':
			case 'ijkl':
			case 'num8426':
			case 'num5123':
			case 'num8456':
				return {
					...createKeyboardMapping(parts[1]),
					...createActionMapping(parts, keyCodeMap)
				}
			case 'gamepad 1':
				return createGamepadMapping(scene, 0)
			case 'gamepad 2':
				return createGamepadMapping(scene, 1)
			case 'mouse 1':
				return createMouseMapping(scene, 0)
			case 'mouse 2':
				return createMouseMapping(scene, 1)
			default:
				console.error('Invalid movement keys')
				return null
		}
	}

}