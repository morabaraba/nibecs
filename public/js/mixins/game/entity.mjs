import { InputProxy, PositionProxy, VelocityProxy, SpriteProxy, RotationProxy } from 'proxies'

/** @class */
export default class GameEntityMixin {

    /**
     * @description Adds an entity to the game.
     * @param {Phaser.GameObjects.Sprite} sprite - The sprite representing the entity.
     * * @param {?} components - The ecs components to add to the entity.
     * @returns {number} The index of the added entity.
     */
    addEntity(sprite, components) {
        this.sprites.push(sprite);
        const eid = this.world.newEntity(components);
        if (this.sprites.length - 1 == eid) {
            sprite.eid = eid
            return eid;
        } else {
            //console.error('this.sprites.length - 1 == eid', this.sprites.length - 1, eid);
            alert('Stopping Game: Game manager entites and world entites de-sync.')
            throw Error('addEntity failed')
        }
    }

    addPhysicsSprite(texture, frame, components) {
        return this.addEntity(this.scene.physics.add.sprite(-100, -100, texture, frame), components)
    }

    addSprite(texture, frame, components) {
        return this.addEntity(this.scene.add.sprite(-100, -100, texture, frame), components)
    }

    createProxies(eid) {
        // setup proxies
        const obj = {}
        obj.input = new InputProxy(eid);
        obj.position = new PositionProxy(eid);
        obj.rotation = new RotationProxy(eid);
        obj.sprite = new SpriteProxy(eid);
        return obj
    }
    /**
     * @description Destroys an entity in the game.
     * @param {number} idx - The index of the entity to destroy.
     */
    destroyEntity(idx) {
        const entity = this.sprites[idx];
        if (entity) {
            entity.destroy();
            this.sprites[idx] = null;
        }
    }
}