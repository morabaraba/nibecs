import {
	createWorld,
	addEntity,
	addComponent,
	defineSerializer,
	defineDeserializer,
	DESERIALIZE_MODE
} from 'thirdparty/bitecs'

import { Position, Velocity, Rotation } from 'components/movement'
import { Sprite } from 'components/entity'

/** @type {Phaser.Scene} */
let scene // Forward declaration of scene to get rid of this.
/** @type {Phaser.Plugins.PluginManager} */
let pluginManager
/** @type {string} */
let pluginName

/**
 * @class GameWorldMixin
 * GameWorldMixin class that provides functionality for creating and managing a bitECS world.
 */
export default class GameWorldMixin {
	/**
	 * Initializes the GameWorldMixin.
	 * @param {Phaser.Scene} scene_ - The scene that owns the plugin.
	 * @param {Phaser.Plugins.PluginManager} pluginManager_ - The plugin manager that manages the plugin.
	 * @param {string} pluginName_ - The name of the plugin.
	 */
	constructorMixin(scene_, pluginManager_, pluginName_) {
		scene = scene_
		pluginManager = pluginManager_
		pluginName = pluginName_
	}

	/**
	 * Creates a new bitECS world object.
	 * @param {Array} [defaultComponents=[Position, Velocity, Rotation, Sprite]] - The default components to add to new entities.
	 * @returns {Object} The created world object.
	 */
	newWorld(defaultComponents = [Position, Velocity, Rotation, Sprite]) {
		/** @type {Object} bitECS World */
		const world = createWorld()

		/**
		 * Creates an entity in the world with default components.
		 * @param {Array} [components] - Components to override default components.
		 * @returns {number} Entity ID
		 */
		world.newEntity = (components) => {
			let eid = addEntity(world)
			if (!components) {
				if (defaultComponents) {
					defaultComponents.forEach(component => {
						addComponent(world, component, eid)
					})
				}
			} else {
				components.forEach(component => {
					addComponent(world, component, eid)
				})
			}
			return eid
		}
		return world
	}

	/**
	 * Serializes components into a byte buffer.
	 * @param {Object} world - The ECS world.
	 * @param {Array} components - The components to serialize.
	 * @returns {ArrayBuffer} The serialized byte buffer.
	 */
	serializeToByteBuffer(world, components) {
		const buffers = []
		components.forEach(component => {
			const name = component.name
			const nameBytes = new TextEncoder().encode(name)
			const data = []

			for (let i = 0; i < world.entities.length; i++) {
				const entity = world.entities[i]
				if (world.hasComponent(entity, component)) {
					for (const key in component) {
						data.push(component[key][entity])
					}
				}
			}

			const dataBytes = new Int32Array(data).buffer
			const buffer = new Uint8Array(
				4 + 4 + nameBytes.length + 4 + dataBytes.byteLength
			)

			let offset = 0
			new DataView(buffer.buffer).setUint32(offset, nameBytes.length, true)
			offset += 4
			buffer.set(nameBytes, offset)
			offset += nameBytes.length
			new DataView(buffer.buffer).setUint32(offset, data.length, true)
			offset += 4
			new Uint8Array(buffer.buffer, offset, dataBytes.byteLength).set(new Uint8Array(dataBytes))

			buffers.push(buffer)
		})

		const totalLength = buffers.reduce((sum, buf) => sum + buf.byteLength, 0)
		const finalBuffer = new Uint8Array(4 + totalLength)
		new DataView(finalBuffer.buffer).setUint32(0, components.length, true)
		let offset = 4
		buffers.forEach(buf => {
			finalBuffer.set(buf, offset)
			offset += buf.byteLength
		})

		return finalBuffer.buffer
	}

	/**
	 * Deserializes components from a byte buffer.
	 * @param {Object} world - The ECS world.
	 * @param {Array} components - The components to deserialize.
	 * @param {ArrayBuffer} buffer - The byte buffer to deserialize.
	 */
	deserializeFromByteBuffer(world, components, buffer) {
		const view = new DataView(buffer)
		let offset = 0

		const componentCount = view.getUint32(offset, true)
		offset += 4

		for (let i = 0; i < componentCount; i++) {
			const nameLen = view.getUint32(offset, true)
			offset += 4
			const name = new TextDecoder().decode(new Uint8Array(buffer, offset, nameLen))
			offset += nameLen

			const dataLen = view.getUint32(offset, true)
			offset += 4
			const data = new Int32Array(new Uint8Array(buffer, offset, dataLen * 4).buffer)
			offset += dataLen * 4

			const component = components.find(comp => comp.name === name)

			if (component) {
				for (let j = 0; j < data.length; j++) {
					const entity = world.entities[j]
					if (world.hasComponent(entity, component)) {
						let idx = 0
						for (const key in component) {
							component[key][entity] = data[idx++]
						}
					}
				}
			}
		}
	}

	/**
	 * Serializes the game world.
	 * @returns {ArrayBuffer} The serialized game world data.
	 */
	serializeGameWorld() {
		let packet = scene.serializeWorld()
		return packet
	}

	/**
	 * Deserializes the game world from the provided packet.
	 * @param {ArrayBuffer} packet - The packet containing serialized game world data.
	 * @returns {Object} The deserialized entities.
	 */
	deserializeGameWorld(packet) {
		let deserializedEnts = scene.deserializeWorld(packet)
		console.log('deserializeGameWorld', deserializedEnts)
		return deserializedEnts
	}

	/**
	 * Sends the world tick data.
	 * @param {number} tick - The current tick.
	 * @param {number} tick_time - The time for the tick.
	 */
	sendWorldTick(tick, tick_time) {
		const pingTime = new Date().getTime()
		const data = this.serializeGameWorld()
		const prefix = ["tck", pingTime, tick] // 3 char str type, number timestamp, number tick
		const packet = new Int8Array(data)
		const message = scene.mq.encodeMessage(prefix, packet)
		message.destinationName = scene.mq.joinTopic
		if (scene.mq.data.isConnected) {
			scene.mq.client.send(message)
		}
		//console.debug("Tick ", tick, "time", tick_time, "sent at: " + pingTime, 'msg length', message.payloadBytes.length, ' to ', message.destinationName, ' msg ', message.payloadBytes)
	}

	/**
	 * Handles the arrival of host messages.
	 * @param {Object} data - The data received from the host.
	 */
	hostMessageArrive(data) {
		// GAH in mq context
		if (scene.mq.hostTopic) {
			scene.gm.state.processTickTimePrev = scene.gm.state.processTickTime
			scene.gm.state.processTickTime = data.latency
		} else {
			scene.gm.deserializeGameWorld(data.packet.buffer)
		}
	}
}
