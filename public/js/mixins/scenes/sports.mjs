/** global Phaser */
// our Entity Component System
import {
	DESERIALIZE_MODE
} from 'thirdparty/bitecs'
// ECS Components
import { Position, Velocity, Rotation } from 'components/movement'
import { Input, UserInput, NpcInput, NetInput } from 'components/input'
import { Sprite } from 'components/entity'
// Utils etc
import { hexColorStringToInt } from 'utils'


/** @type {Phaser.Scene} */
let scene // Forward declaration of scene to get rid of `this` in codebase.
let state
/**
 * @class SportsSceneMixin
 * @extends {Phaser.Scene}
 * @description The SportsSceneMixin class extends Phaser.Scene and provides methods
 * to create and manage team players, including both user-controlled and NPCs.
 */
export default class SportsSceneMixin {

	/**
	 * Initializes the scene.
	 */
	constructorMixin() {
	}

	createMixin() {
		scene = this
		state = scene.data.get('state')
		state.gameMode = scene.gameData.settings[`game-mode`]
		state.goalDelay = Number(scene.gameData.settings[`game-goal-delay`])
		state.teamName = [scene.gameData.settings[`game-left-name`], scene.gameData.settings[`game-right-name`]]
		state.teamColor = [scene.gameData.settings[`game-left-color`], scene.gameData.settings[`game-right-color`]]
		state.halfTimeDuration = Number(scene.gameData.settings[`game-half-time`])
		state.halfTime = false
		state.showFps = Number(scene.gameData.settings[`game-show-fps`] || 1)
		state.totalTime = state.halfTimeDuration * 2
		scene.timerText = null
		scene.timerEvent = null
		state.elapsedTime = 0
		state.gameOver = false
		state.physics = { players: scene.physics.add.group() }
	}

	/**
	 * a Team consist out of 6 players and 2 teams.
	 *
	 * Call allPlayersToScene to
	 *
	 * creates team players, assigns positions, and manages input keys.
	 * It also includes logic to handle both user-controlled and NPC players.
	 */
	createTeamPlayers() {
		// Initialize the number of player positions (6 positions per team)
		const players = 6 * 2
		scene.allPlayersToScene(players)
	}

	/**
	 * From the Game State create player from their Data and State,
	 * Like Sprites, Containers, Phaser key mappings, npc behaviour etc.
	 */
	allPlayersToScene(totalPlayers) {
		// helper arrays on the scene to easly in systems access player data and state
		scene.playerData = []
		scene.playerState = []
		for (let idx = 0; idx < totalPlayers; idx++) {
			// create basic data and state object per player index from game state
			const obj = scene.createPlayerIndex(idx)
			// push them into the scene proxies/helper arrays
			const data = obj[0]
			const state = obj[1]
			scene.playerData.push(data)
			scene.playerState.push(state)
			if (obj[0] !== false) { // if it not disabled
				// create player sprite and populate state with more info from data
				scene.createPlayerIndexStateFromData(idx)
			}

		}
	}

	/**
	 * Generates a player object with input keys, team position, and team information.
	 * @param {number} idx - The index of the player.
	 * @returns {Object} The player object containing keys, teamPosition, and team information.
	 */
	createPlayerIndex(idx) {
		const data = {
			nick: scene.gameData.settings[`player-${idx}-nick`].replace('| ', ''),
			keys: scene.gm.selectKeys2PlayerKeys(scene.gameData.settings[`player-${idx}-keys`], scene),
			teamPosition: scene.gameData.settings[`player-${idx}-role`].replace('| ', ''),
			team: scene.gameData.settings[`player-${idx}-team`].replace('| ', ''),
			spriteFrame: scene.gameData.settings[`player-${idx}-sprite`].replace('| ', ''),
		}
		if (!data.keys) {
			return [false, false]
		}
		const state = {
			keys: null,
		}
		if (data.keys && data.keys.keysCreated) {
			state.keys = data.keys
			data.keys = state.keys.name // for now patch data back to be able to json serialize
		} else {
			state.keys = scene.input.keyboard.addKeys(data.keys)
			state.keys = state.keys
		}
		return [data, state]
	}

	/**
	 * Creates a player and sets their initial position and properties.
	 * Can handle both user-controlled players and NPCs.
	 *
	 * @param {number} idx - The index of the player.
	 */
	createPlayerIndexStateFromData(idx) {

		// Create player sprite and position
		const playerSprite = scene.add.sprite(0, 0, scene.spriteSheetName, Phaser.Math.Between(idx, idx + 14))
		//const playerSprite = scene.physics.add.sprite(0, 0, scene.spriteSheetName, Phaser.Math.Between(idx, idx + 14))
		// Create a container to group the sprite and text
		const playerContainer = scene.add.container(-100, -100, [])
		//const playerContainer = scene.physics.add.container(-100, -100, [])
		playerContainer.setDataEnabled()
		const playerData = scene.playerData[idx]
		const keys = playerData.keys
		const npc = keys == 'npc'
		const network = keys == 'network'
		const playerState = scene.playerState[idx]
		// Define components based on whether the player is NPC or not
		let playerComponents = [Position, Velocity, Rotation, Sprite, Input, UserInput]
		if (npc) {
			playerComponents = [Position, Velocity, Rotation, Sprite, Input, NpcInput]
		}
		if (network) {
			playerComponents = [Position, Velocity, Rotation, Sprite, Input, NetInput]
		}
		playerState.components = playerComponents
		playerContainer.state = playerState
		playerContainer.data.set('data', playerData)
		const playerEid = scene.gm.addPlayer(playerContainer)

		// Create player entity and proxies
		const player = scene.gm.sprites[playerEid]
		player.p = scene.gm.createProxies(playerEid)

		// Set player sprite properties
		player.p.sprite.texture = 2
		player.p.sprite.frame = playerData.spriteFrame
		player.p.input.speed = scene.gameData.playerSpeed
		const textColor = playerData.team == 'left' ? state.teamColor[0] : state.teamColor[1]

		// Add text to the left of the player sprite
		const playerText = scene.add.text(-playerSprite.width / 2 + 4, 0, playerData.nick, {
			fontFamily: 'monospace',
			fontStyle: 'bold',
			fontSize: '14px',
			fill: textColor
		}).setOrigin(0.5).setAngle(-90) // Rotate the text to make it look like a soccer shirt name
		playerContainer.add([playerSprite, playerText])
		playerContainer.playerSprite = playerSprite

		if (!playerData.team == 'left') {
			playerSprite.angle = 180 // Rotate sprite for right team
		}

		// Set player position based on the team side
		let placeX
		let placeY
		if (playerData.team == 'left') {
			const place = scene.playerLeftPositions.positions[playerData.teamPosition]
			placeX = place[0]
			placeY = place[1]
		} else {
			const place = scene.playerRightPositions.positions[playerData.teamPosition]
			placeX = place[0]
			placeY = place[1]
		}
		player.p.position.x = placeX
		player.p.position.y = placeY

		// Update the container position to match the player position
		playerContainer.setPosition(player.p.position.x, player.p.position.y)

		// Store container object for further manipulation if needed
		player.container = playerContainer
	}
	/**
	 * Creates player positions based on the side of the field.
	 *
	 * @param {string} side - The side of the field ('left' or 'right'). Default is 'left'.
	 * @returns {Object} An object containing the positions.
	 */
	createPlayerPositions(side = 'left') {
		const scene = this
		const bounds = scene.bounds

		const isRightSide = side === 'right'

		const calcWidthFromLeftRight = bounds.right - bounds.left
		const calcHeightFromTopBottom = bounds.bottom - bounds.top

		// Define position calculation constants
		let goalKeeperX = bounds.left + Phaser.Math.Between(30, 150)
		let goalKeeperY = bounds.top + calcHeightFromTopBottom / 2

		const defender1BaseX = bounds.left + calcWidthFromLeftRight / 8
		let defender1X = Phaser.Math.Between(defender1BaseX, defender1BaseX + 60)
		let defender1Y = bounds.top + calcHeightFromTopBottom / 4 + Phaser.Math.Between(-30, 30)

		const defender2BaseX = bounds.left + calcWidthFromLeftRight / 8
		let defender2X = Phaser.Math.Between(defender2BaseX, defender2BaseX + 60)
		let defender2Y = bounds.top + 3 * calcHeightFromTopBottom / 4 + Phaser.Math.Between(-30, 30)

		const midfielderBaseX = bounds.left + (calcWidthFromLeftRight / 8) * 2.4
		let midfielderX = Phaser.Math.Between(midfielderBaseX, midfielderBaseX + 60)
		let midfielderY = bounds.top + calcHeightFromTopBottom / 4 + Phaser.Math.Between(-10, 10)

		const forward1BaseX = bounds.left + (calcWidthFromLeftRight / 8) * 2.9
		let forward1X = Phaser.Math.Between(forward1BaseX, forward1BaseX + 10)
		let forward1Y = goalKeeperY

		const forward2BaseX = bounds.left + (calcWidthFromLeftRight / 8) * 3.2
		let forward2X = Phaser.Math.Between(forward2BaseX, forward2BaseX + 60)
		let forward2Y = bounds.top + 3 * calcHeightFromTopBottom / 4 + Phaser.Math.Between(-30, 30)

		// Adjust positions for right side with some randomness
		if (isRightSide) {
			const rightOffset = (calcWidthFromLeftRight / 2)
			goalKeeperX = bounds.left + (calcWidthFromLeftRight / 10) * 9 + Phaser.Math.Between(0, 60)
			defender1X += rightOffset * 1.3
			defender2X += rightOffset * 1.3
			midfielderX = bounds.left + (calcWidthFromLeftRight / 10) * 6.8 + Phaser.Math.Between(-30, 30)
			midfielderY = bounds.top + calcHeightFromTopBottom / 1.3 + Phaser.Math.Between(-10, 10)
			forward1X = bounds.left + (calcWidthFromLeftRight / 10) * 6.5 + Phaser.Math.Between(-25, -15)
			forward2X = bounds.left + (calcWidthFromLeftRight / 10) * 6.2 + Phaser.Math.Between(-60, 60)
			forward2Y = bounds.top + 1 * calcHeightFromTopBottom / 4 + Phaser.Math.Between(-30, 30)
		}

		// Define positions for players
		const positions = {
			'forward 1': [forward1X, forward1Y],
			'forward 2': [forward2X, forward2Y],
			'midfielder': [midfielderX, midfielderY],
			'defender 1': [defender1X, defender1Y],
			'defender 2': [defender2X, defender2Y],
			'goalkeeper': [goalKeeperX, goalKeeperY],
		}

		return positions
	}


	/**
	 * Creates and returns player starting positions for either left or right side of the field.
	 * Positions include goalkeeper, defenders, midfielder, and forwards.
	 * Also creates visual representation for the positions.
	 *
	 * @param {string} side - The side of the field ('left' or 'right'). Default is 'left'.
	 * @returns {Object} An object containing the container with visual elements and the positions.
	 */
	createPlayerStartPosition(side = 'left') {
		const scene = this
		const positions = this.createPlayerPositions(side)
		const isRightSide = side === 'right'

		const color = isRightSide ? hexColorStringToInt(state.teamColor[1]) : hexColorStringToInt(state.teamColor[0])
		const textColor = isRightSide ? state.teamColor[1] : state.teamColor[0]

		// Create a container to hold the visual elements
		const container = scene.add.container()

		// Add circles and position texts for each player position
		for (let position in positions) {
			const [x, y] = positions[position]
			const circle = scene.add.circle(x, y, 24, color, 0.20).setOrigin(0.5) // 20% transparent
			const text = scene.add.text(x, y - 35, position, { font: '16px monospace', fill: textColor, fontStyle: 'bold' }).setOrigin(0.5) // Text higher and styled with color correction

			container.add([circle, text])
		}

		// Store the elements in scene.startPositions
		if (!scene.startPositions) {
			scene.startPositions = { left: [], right: [] }
		}
		scene.startPositions[side].push({ container, positions })

		// Return the container and positions
		const result = { container, positions }
		return result
	}

	/**
	 * Updates player starting positions to switch colors after halftime.
	 */
	updatePlayerStartPosition() {
		// Loop through all positions and update their colors
		for (let side in scene.startPositions) {
			const isRightSide = side === 'right'
			const textColor = isRightSide ? state.teamColor[0] : state.teamColor[1]
			const color = isRightSide ? hexColorStringToInt(state.teamColor[0]) : hexColorStringToInt(state.teamColor[1])
			scene.startPositions[side].forEach(({ container }) => {
				container.iterate((child) => {
					if (child.type === 'Text') {
						child.setFill(textColor)
					} else if (child.type === 'Arc') { // Circle
						child.setFillStyle(color, 0.20)
					}
				})
			})
		}
	}


	/**
	 * Flashes the "GOAL" message in the center of the screen with random bright colors.
	 * @param {string} postfixText - The team name and score post fix after GOAL!
	 * @param {number} [duration=3000] - The total duration for which the "GOAL" message will be displayed (in milliseconds).
	 * @param {number} [flashes=3] - The number of times the color of the "GOAL" message will change.
	 */
	flashGoal(postfixText, duration = state.goalDelay, flashes = 6) {
		scene.playGoalSound()
		scene.goalSystem(scene.gm.world)

		const screenHeight = scene.screenHeight
		const screenWidth = scene.screenWidth
		const goalText = scene.add.text(
			screenWidth / 2,
			screenHeight / 2,
			`GOAL ${postfixText}!`,
			{
				fontSize: '64px',
				fill: '#ffffff',
				align: 'center'
			}
		)
		goalText.setOrigin(0.5, 0.5) // Center the text

		// Function to generate a random bright color
		function getRandomBrightColor() {
			const letters = '0123456789ABCDEF'
			let color = '#'
			for (let i = 0; i < 6; i++) {
				color += letters[Math.floor(Math.random() * 16)]
			}
			return color
		}

		const totalFlashes = flashes * 3
		const flashInterval = duration / totalFlashes
		let flashCount = 0

		// Flash the text with random colors at intervals
		const flashTimer = setInterval(() => {
			if (flashCount >= totalFlashes) {
				clearInterval(flashTimer)
				goalText.destroy() // Remove the text
			} else {
				const randomColor = getRandomBrightColor()
				goalText.setColor(randomColor)
				flashCount++
			}
		}, flashInterval)
	}

	/**
	 * Creates and initializes the ball entity and its properties.
	 * Sets up the ball sprite and uses physics if the game is hosted.
	 */
	createBall() {
		// Setup ball Sprite and Entity
		let ball
		const ballComponents = [Position, Velocity, Rotation, Sprite]

		if (scene.gameData.isHost) { // If host, use Phaser physics to move the ball sprite
			ball = scene.ball = scene.gm.sprites[scene.gm.addPhysicsSprite('ball', 1, ballComponents)]
			ball.setBounce(1) // Ball bounces off surfaces
			ball.setCollideWorldBounds(true) // Ball collides with world bounds
		} else { // If client, get position from tick
			ball = scene.ball = scene.gm.sprites[scene.gm.addSprite('ball', 1, ballComponents)]
		}

		ball.p = scene.gm.createProxies(ball.eid)
		ball.x = scene.screenWidth / 2 // Set ball position to center of screen
		ball.y = scene.screenHeight / 2
	}

	/**
	 * Updates the ball's position, velocity, and handles collision detection.
	 * Manages goal scoring and resetting the ball.
	 */
	updateBall() {
		const ball = scene.ball
		const ballEid = ball.eid
		const bounds = scene.bounds
		const goalWidth = 100 - 6 // Width of the goal minus the goal posts
		const screenHeight = scene.screenHeight

		// Update velocity
		if (Velocity.x[ball.eid] > 0) {
			Velocity.x[ball.eid] -= 1
		}
		if (Velocity.y[ball.eid] > 0) {
			Velocity.y[ball.eid] -= 1
		}

		// Check if the ball is near the left or right bounds (might be a goal)
		if (ball.x < bounds.left || ball.x > bounds.right) {
			// Check if the ball is in the goal area
			if (ball.y > (screenHeight / 2 - goalWidth / 2) && ball.y < (screenHeight / 2 + goalWidth / 2)) {
				if (ball.x >= bounds.right) { // first half right scoring goes to left
					if (state.halfTime) {
						scene.gameState.score[1] += 1 // Right side scores
						scene.flashGoal(state.teamName[1] + ' ' + scene.gameState.score[1])
					} else {
						scene.gameState.score[0] += 1 // Left side scores
						scene.flashGoal(state.teamName[0] + ' ' + scene.gameState.score[0])
					}
				} else if (ball.x <= bounds.left) { // first half left scoring goes to right
					if (state.halfTime) {
						scene.gameState.score[0] += 1 // Left side scores
						scene.flashGoal(state.teamName[0] + ' ' + scene.gameState.score[0])
					} else {
						scene.gameState.score[1] += 1 // Right side scores
						scene.flashGoal(state.teamName[1] + ' ' + scene.gameState.score[1])
					}
				}
				scene.resetBall() // Reset ball position
				scene.gm.pause(true, scene.gameState.goalDelay) // Pause the game for 3 seconds
			}
		}

		// Prevent the ball from moving out of the bounds and handle post hits
		if (ball.x < bounds.left) {
			Position.x[ballEid] = bounds.left + (ball.width / 2)
			ball.x = Position.x[ballEid]
			Velocity.x[ballEid] *= -1
			scene.playPostHitSound()
		}
		if (ball.x > bounds.right) {
			Position.x[ballEid] = bounds.right - (ball.width / 2)
			ball.x = Position.x[ballEid]
			Velocity.x[ballEid] *= -1
			scene.playPostHitSound()
		}
		if (ball.y < bounds.top) {
			Position.y[ballEid] = bounds.top + (ball.width / 2)
			ball.y = Position.y[ballEid]
			Velocity.y[ballEid] *= -1
			scene.playPostHitSound()
		}
		if (ball.y > bounds.bottom) {
			Position.y[ballEid] = bounds.bottom - (ball.width / 2)
			ball.y = Position.y[ballEid]
			Velocity.y[ballEid] *= -1
			scene.playPostHitSound()
		}

		// Sync ball state between bitECS and Phaser
		ball.body.velocity.x = Velocity.x[ball.eid]
		ball.body.velocity.y = Velocity.y[ball.eid]
		ball.angle = Rotation.angle[ball.eid]
		Position.x[ballEid] = ball.x
		Position.y[ballEid] = ball.y
	}

	/**
	 * Resets the ball's position to the center of the screen and stops its movement.
	 */
	resetBall() {
		const screenHeight = scene.screenHeight
		const screenWidth = scene.screenWidth
		const ball = scene.ball
		const ballEid = ball.eid

		// Reset ball position to the center
		Position.x[ballEid] = screenWidth / 2
		Position.y[ballEid] = screenHeight / 2
		Velocity.x[ball.eid] = 0
		Velocity.y[ball.eid] = 0
		ball.x = Position.x[ballEid]
		ball.y = Position.y[ballEid]
		ball.body.velocity.x = Velocity.x[ball.eid]
		ball.body.velocity.y = Velocity.y[ball.eid]
	}

	/**
	 * Creates a soccer field with markings including center circle, goal areas, and corner arcs.
	 * @class
	 * @param {Phaser.Scene} scene - The Phaser scene.
	 */
	createSoccerField() {
		const screenWidth = scene.cameras.main.width
		const screenHeight = scene.cameras.main.height

		// Create the field bounds shape
		const boundsShape = scene.add.rectangle(
			(screenWidth / 2) + 5,
			screenHeight / 2,
			(screenWidth - 64) - 80,
			screenHeight - 64,
			0x228B22
		).setOrigin(0.5)

		// Ensure the shape is below other game objects
		boundsShape.setDepth(-1)

		// Store bounds for easier access
		scene.bounds = {
			left: boundsShape.x - boundsShape.width / 2,
			right: boundsShape.x + boundsShape.width / 2,
			top: boundsShape.y - boundsShape.height / 2,
			bottom: boundsShape.y + boundsShape.height / 2
		}

		// Calculate the width for each grass line
		const fieldWidth = scene.bounds.right - scene.bounds.left
		const grassWidthPerLine = fieldWidth / 10
		const grassColors = [0x006400, 0x228B22] // Two shades of green resembling grass

		// Create vertical grass bars within the field bounds
		const grassGraphics = scene.add.graphics()

		for (let i = 0; i < 10; i++) {
			grassGraphics.fillStyle(grassColors[i % 2])
			grassGraphics.fillRect(
				scene.bounds.left + i * grassWidthPerLine,
				scene.bounds.top,
				grassWidthPerLine,
				boundsShape.height
			)
		}

		// Draw the striped border (fence-like) with goal post openings
		const fenceGraphics = scene.add.graphics()
		const stripeWidth = 4
		const fenceColors = [0x2f4f4f, 0xa9a9a9] // Dark gray and light gray
		const goalPostWidth = 100

		// Top and bottom borders
		for (let i = 0; i < boundsShape.width / stripeWidth; i++) {
			fenceGraphics.fillStyle(fenceColors[i % 2])
			fenceGraphics.fillRect(
				scene.bounds.left + i * stripeWidth,
				scene.bounds.top - stripeWidth,
				stripeWidth,
				stripeWidth
			)
			fenceGraphics.fillRect(
				scene.bounds.left + i * stripeWidth,
				scene.bounds.bottom,
				stripeWidth,
				stripeWidth
			)
		}

		// Left border with goal post opening
		for (let i = 0; i < boundsShape.height / stripeWidth; i++) {
			fenceGraphics.fillStyle(fenceColors[i % 2])
			if (i * stripeWidth <= (screenHeight - (goalPostWidth * 1.7)) / 2 ||
				i * stripeWidth >= (screenHeight + (goalPostWidth * 0.4)) / 2) {
				fenceGraphics.fillRect(
					scene.bounds.left - stripeWidth,
					scene.bounds.top + i * stripeWidth,
					stripeWidth,
					stripeWidth
				)
			}
		}

		// Right border with goal post opening
		for (let i = 0; i < boundsShape.height / stripeWidth; i++) {
			fenceGraphics.fillStyle(fenceColors[i % 2])
			if (i * stripeWidth < (screenHeight - (goalPostWidth * 1.7)) / 2 ||
				i * stripeWidth > (screenHeight + (goalPostWidth * 0.4)) / 2) {
				fenceGraphics.fillRect(
					scene.bounds.right,
					scene.bounds.top + i * stripeWidth,
					stripeWidth,
					stripeWidth
				)
			}
		}

		// Draw field lines
		const graphics = scene.add.graphics({ lineStyle: { width: 2, color: 0xFFFFFF } })

		// Vertical half-line
		graphics.beginPath()
		graphics.moveTo(boundsShape.x, scene.bounds.top)
		graphics.lineTo(boundsShape.x, scene.bounds.bottom)
		graphics.strokePath()

		// Center circle
		const centerX = boundsShape.x
		const centerY = boundsShape.y
		const centerCircleRadius = 75
		graphics.strokeCircle(centerX, centerY, centerCircleRadius)

		// Small start circle in the center
		const smallCenterCircleRadius = 5
		graphics.fillStyle(0xFFFFFF)
		graphics.fillCircle(centerX, centerY, smallCenterCircleRadius)

		// Goal areas
		const goalAreaHeight = 150
		const goalAreaDepth = 60
		const goalOuterRatio = 2.5

		// Left small goal area
		graphics.strokeRect(scene.bounds.left, (screenHeight - goalAreaHeight) / 2, goalAreaDepth, goalAreaHeight)
		// Right small goal area
		graphics.strokeRect(scene.bounds.right - goalAreaDepth, (screenHeight - goalAreaHeight) / 2, goalAreaDepth, goalAreaHeight)

		// Left larger goal area
		graphics.strokeRect(scene.bounds.left, (screenHeight - (goalAreaHeight * goalOuterRatio)) / 2, goalAreaDepth * goalOuterRatio, (goalAreaHeight * goalOuterRatio))
		// Right larger goal area
		graphics.strokeRect(scene.bounds.right - (goalAreaDepth * goalOuterRatio), (screenHeight - (goalAreaHeight * goalOuterRatio)) / 2, goalAreaDepth * goalOuterRatio, (goalAreaHeight * goalOuterRatio))

		// Semi-circles in front of larger goal areas
		const penaltyArcRadius = 50
		graphics.beginPath()
		graphics.arc(scene.bounds.left + goalAreaDepth * goalOuterRatio, screenHeight / 2, penaltyArcRadius, 1.5 * Math.PI, 0.5 * Math.PI)
		graphics.strokePath()
		graphics.beginPath()
		graphics.arc(scene.bounds.right - goalAreaDepth * goalOuterRatio, screenHeight / 2, penaltyArcRadius, 0.5 * Math.PI, 1.5 * Math.PI)
		graphics.strokePath()

		// Corner arcs
		const cornerArcRadius = 10
		graphics.beginPath()
		graphics.arc(scene.bounds.left, scene.bounds.top, cornerArcRadius, 0, 0.5 * Math.PI)
		graphics.strokePath()
		graphics.beginPath()
		graphics.arc(scene.bounds.right, scene.bounds.top, cornerArcRadius, 0.5 * Math.PI, Math.PI)
		graphics.strokePath()
		graphics.beginPath()
		graphics.arc(scene.bounds.left, scene.bounds.bottom, cornerArcRadius, 1.5 * Math.PI, 2 * Math.PI)
		graphics.strokePath()
		graphics.beginPath()
		graphics.arc(scene.bounds.right, scene.bounds.bottom, cornerArcRadius, Math.PI, 1.5 * Math.PI)
		graphics.strokePath()

		// Complete the drawing
		graphics.strokePath()
	}

	/**
	 * Creates a visual representation of a soccer goal on the screen.
	 * Draws the goal posts, crossbar, and net with specified rotation and position.
	 *
	 * @param {number} x - The x-coordinate of the goal's pivot point.
	 * @param {number} y - The y-coordinate of the goal's pivot point.
	 * @param {number} goalRotation - The rotation angle of the goal in degrees.
	 */
	createSoccerGoal(x, y, goalRotation) {
		const graphics = scene.add.graphics({ fillStyle: { color: 0x8B4513 } }) // Brown color

		// Define the goal dimensions
		const goalWidth = 100
		const goalHeight = 50
		const postThickness = 5
		const crossbarThickness = 5

		// Calculate the pivot point (example: center of the goal)
		const pivotX = x + goalWidth / 2
		const pivotY = y + goalHeight / 2

		// Draw left post
		graphics.fillRect(x - pivotX, y - pivotY, postThickness, goalHeight)

		// Draw right post
		graphics.fillRect(x + goalWidth - postThickness - pivotX, y - pivotY, postThickness, goalHeight)

		// Draw crossbar
		graphics.fillRect(x - pivotX, y - pivotY, goalWidth, crossbarThickness)

		// Draw net as grid pattern
		graphics.lineStyle(2, 0xD3D3D3, 1) // Light gray color for the net
		graphics.beginPath()
		const netDensity = 5 // Number of lines in each direction

		// Vertical lines (inside the goal area)
		for (let i = 1; i < netDensity; i++) {
			const xPos = x + (i * (goalWidth - postThickness * 2)) / netDensity + postThickness - pivotX
			graphics.moveTo(xPos, y + crossbarThickness - pivotY)
			graphics.lineTo(xPos, y + goalHeight - pivotY)
		}

		// Horizontal lines (inside the goal area)
		for (let j = 1; j < netDensity; j++) {
			const yPos = y + (j * (goalHeight - crossbarThickness)) / netDensity + crossbarThickness - pivotY
			graphics.moveTo(x + postThickness - pivotX, yPos)
			graphics.lineTo(x + goalWidth - postThickness - pivotX, yPos)
		}
		graphics.strokePath()

		// Apply rotation
		graphics.setRotation(Phaser.Math.DegToRad(goalRotation))
		graphics.setPosition(pivotX, pivotY) // Position graphics at the new pivot point
	}

	/**
	 * Serializes the game world if the serialize function is available.
	 *
	 * @returns {Object} The serialized world packet.
	 */
	serializeWorld() {
		if (!scene.serialize) return // Exit if serialize function is not available
		const packet = scene.serialize(scene.gm.world) // Serialize the game world
		return packet // Return the serialized packet
	}

	/**
	 * Deserializes the provided packet to the game world if the deserialize function is available.
	 *
	 * @param {Object} packet - The packet containing the serialized world data.
	 * @returns {Array} The deserialized entities.
	 */
	deserializeWorld(packet) {
		if (!scene.deserialize) return // Exit if deserialize function is not available
		const mode = DESERIALIZE_MODE.REPLACE // Set deserialization mode (REPLACE, MAP, or APPEND)
		scene.deserializedEnts = scene.deserialize(scene.gm.world, packet, mode) // Deserialize the packet
		return scene.deserializedEnts // Return the deserialized entities
	}

	/**
	 * Initializes and creates sound objects for the game.
	 */
	createSound() {
		scene.goalSound = scene.sound.add('goal')
		scene.hitPostSound = scene.sound.add('hitPost')
		scene.runningGrassSound = scene.sound.add('runningGrass')

		// Save kick sounds into an array
		scene.kicks = [
			scene.sound.add('kick1'),
			scene.sound.add('kick2'),
			scene.sound.add('kick3')
		]
	}

	/**
	 * Plays the goal sound and gradually fades it out before stopping.
	 */
	playGoalSound() {
		scene.goalSound.play()
		scene.time.delayedCall(scene.goalSound.duration * 1000 - 1000, () => {
			scene.tweens.add({
				targets: scene.goalSound,
				volume: 0,
				ease: 'Linear',
				duration: 1000,
				onComplete: () => {
					scene.goalSound.stop()
					scene.goalSound.setVolume(1)  // Reset volume for next play
				}
			})
		})
	}

	/**
	 * Plays the sound effect when the ball hits the post, ensuring it doesn't overlap.
	 */
	playPostHitSound() {
		if (!scene.hitPostSound.isPlaying) {
			scene.hitPostSound.play()
		}
	}

	playRunSound(playRunSound) {
		if (playRunSound) {
			if (!scene.runningGrassSound.isPlaying) {
				scene.runningGrassSound.play()
			}
		} else {
			if (scene.runningGrassSound.isPlaying) {
				scene.runningGrassSound.stop()
			}
		}
	}

	checkPlayerOverlap() {
		const sprites = scene.gm.sprites
		const maxIter = 1
		let iterCount = 0

		const pushBackX = 10 // Additional horizontal space
		const pushBackY = 10 // Additional vertical space

		const resolveOverlap = (spriteA, spriteB) => {
			const boundsA = spriteA.getBounds()
			const boundsB = spriteB.getBounds()

			const overlapX = Math.abs(boundsA.centerX - boundsB.centerX)
			const overlapY = Math.abs(boundsA.centerY - boundsB.centerY)

			if (overlapX > overlapY) {
				if (boundsA.centerX < boundsB.centerX) {
					spriteB.x = boundsA.right + spriteB.width / 2 + pushBackX
				} else {
					spriteB.x = boundsA.left - spriteB.width / 2 - pushBackX
				}
				spriteB.p.position.x = spriteB.x
			} else {
				if (boundsA.centerY < boundsB.centerY) {
					spriteB.y = boundsA.bottom + spriteB.height / 2 + pushBackY
				} else {
					spriteB.y = boundsA.top - spriteB.height / 2 - pushBackY
				}
				spriteB.p.position.y = spriteB.y
			}
		}

		do {
			iterCount++
			const overlapSprites = []

			// Find all overlapping sprite pairs
			for (let i = 0; i < sprites.length; i++) {
				for (let j = i + 1; j < sprites.length; j++) {
					if (sprites[i] !== scene.ball && sprites[j] !== scene.ball) {
						const boundsA = sprites[i].getBounds()
						const boundsB = sprites[j].getBounds()
						if (Phaser.Geom.Intersects.RectangleToRectangle(boundsA, boundsB)) {
							overlapSprites.push([sprites[i], sprites[j]])
						}
					}
				}
			}

			// Adjust positions to resolve overlaps
			overlapSprites.forEach(([spriteA, spriteB]) => {
				resolveOverlap(spriteA, spriteB)
			})

			// Check again to confirm there are no more overlaps
			let overlapsExist = false
			for (let i = 0; i < overlapSprites.length; i++) {
				const [spriteA, spriteB] = overlapSprites[i]
				const boundsA = spriteA.getBounds()
				const boundsB = spriteB.getBounds()
				if (Phaser.Geom.Intersects.RectangleToRectangle(boundsA, boundsB)) {
					overlapsExist = true
					break
				}
			}

			if (!overlapsExist) break
		} while (iterCount < maxIter)
	}

	/**
	 * Method to show and update timer text
	 */
	showTimerText() {
		const screenWidth = scene.screenWidth
		// Create scoreboard and timer text
		const scoreBoardText = state.teamName[0] + ' 0 - 0 ' + state.teamName[1]
		scene.scoreBoard = scene.add.text((screenWidth / 2) + 6, 10, scoreBoardText, { color: '#fff', stroke: '#fff' }).setOrigin(0.5)
		scene.timerText = scene.add.text(scene.bounds.left, 10, '--:--.- ⌚', { color: '#fff', stroke: '#fff' }).setOrigin(0, 0.5)
	}

	/**
	 * Method to update scoreBoard
	 */
	updateScoreBoard() {
		let scoreBoardText
		if (scene.gameState.halfTime) {
			scoreBoardText = scene.gameState.teamName[1] + ' ' + scene.gameState.score[1] + ' - ' + scene.gameState.score[0] + ' ' + scene.gameState.teamName[0]
		} else {
			scoreBoardText = scene.gameState.teamName[0] + ' ' + scene.gameState.score[0] + ' - ' + scene.gameState.score[1] + ' ' + scene.gameState.teamName[1]
		}
		scene.scoreBoard.setText(scoreBoardText)
	}

	/**
	 * Method to update timer
	 */
	updateTimer(elapsedTime = false) {
		if (elapsedTime) {
			state.elapsedTime = elapsedTime
			state.lastUpdateTime = scene.time.now
		} else {
			const delta = scene.time.now - state.lastUpdateTime
			state.lastUpdateTime = scene.time.now
			state.elapsedTime += delta // Increase elapsed time by delta time
		}

		const minutes = Math.floor(state.elapsedTime / 60000).toString().padStart(2, '0')
		const seconds = Math.floor((state.elapsedTime % 60000) / 1000).toString().padStart(2, '0')
		const centiseconds = Math.floor((state.elapsedTime % 1000) / 10).toString().padStart(2, '0')

		let timerText = `⌚ ${minutes}:${seconds}.${centiseconds}`
		if (state.showFps) {
			//const fps = Math.round(scene.game.loop.actualFps);
			const fps = scene.game.loop.actualFps.toFixed(2).toString().padStart(2, '0')
			timerText += ` (${fps} fps)`
		}

		scene.timerText.setText(timerText)
	}

	/**
	 * Method to flash half-time message
	 * @param {string} postFixText
	 */
	flashHalfTime(postFixText) {
		scene.gm.pause(true)
		state.halfTime = !state.halfTime;
		scene.updatePlayerStartPosition()
		scene.updateScoreBoard()
		scene.resetBall() // Reset ball position
		scene.goalSystem(scene.gm.world)
		scene.updateTimer(state.halfTimeDuration)
		console.log(postFixText) // For testing purposes
		// Display the half-time message
		const halfTimeText = scene.add.text(
			scene.cameras.main.centerX,
			scene.cameras.main.centerY,
			postFixText,
			{
				font: '32px Arial',
				fill: '#fff',
				align: 'center',
				padding: { x: 10, y: 10 }, // Add padding to create space for the border
				backgroundColor: 'rgba(0, 0, 0, 0.5)' // Set a semi-transparent background to see rounded border effect
			}
		).setOrigin(0.5);

		// Add black rounded border
		halfTimeText.setStroke('#000000', 4); // Set the color and thickness of the stroke (border)
		halfTimeText.setShadow(2, 2, '#000', 2, true, true); // Add shadow to emphasize the border

		// Adjust the background to complete the visual effect
		halfTimeText.setBackgroundColor('#000000');
		halfTimeText.setPadding(4, 4);


		// Remove the message after the goal delay
		scene.time.delayedCall(state.goalDelay * 2, () => {
			state.gameOver = false; // reset game over if in training and halfime reached
			halfTimeText.destroy()
			scene.startSecondHalf()
		})
	}

	/**
	 * Method to start the second half
	 */
	startSecondHalf() {
		const secondHalfDuration = state.halfTimeDuration // Assuming the second half is the same length
		// Set timer for the second half
		const ticks = scene.gm.timeToTick(secondHalfDuration);
		scene.gm.onTick(ticks, () => {

			state.gameOver = true;
			const isDraw = state.score[0] === state.score[1];
			const winningTeam = state.score[0] > state.score[1] ? state.teamName[0] : state.teamName[1];
			const postFixText = '🏆 Results\n' + (isDraw ? "It's A Draw" : (winningTeam + ' Won!')) + '\n' + state.score[0] + ' - ' + state.score[1];
			if (state.gameMode == 'training') {
				scene.flashHalfTime(postFixText)
			} else {
				scene.flashResult(postFixText);
			}

		});
		scene.gm.pause(false)
		scene.updateTimer(state.halfTimeDuration) // to set lastUpdateTime correctly
	}



	/**
	 * Method to flash result message
	 * @param {string} postFixText
	 */
	flashResult(postFixText) {
		scene.gm.pause(true);
		scene.resetBall(); // Reset ball position
		scene.goalSystem(scene.gm.world);
		console.log(postFixText); // For testing purposes

		// Display the result message
		const resultText = scene.add.text(
			scene.cameras.main.centerX,
			scene.cameras.main.centerY,
			postFixText,
			{
				font: '32px Arial',
				fill: '#fff',
				align: 'center',
				padding: { x: 10, y: 10 }, // Add padding to create space for the border
				backgroundColor: 'rgba(0, 0, 0, 0.5)' // Set a semi-transparent background to see rounded border effect
			}
		).setOrigin(0.5);

		// Add black rounded border
		resultText.setStroke('#000000', 4); // Set the color and thickness of the stroke (border)
		resultText.setShadow(2, 2, '#000', 2, true, true); // Add shadow to emphasize the border

		// Adjust the background to complete the visual effect
		resultText.setBackgroundColor('#000000');
		resultText.setPadding(4, 4);

		// Show "Done" button after 3 seconds
		scene.time.delayedCall(3000, () => {
			const doneButton = scene.add.text(
				scene.cameras.main.centerX,
				scene.cameras.main.centerY + resultText.height + 8, // Position below the result text
				'Done',
				{
					font: '24px Arial',
					fill: '#fff',
					backgroundColor: '#000',
					padding: { x: 10, y: 10 }
				}
			).setOrigin(0.5);

			// Add interactivity to the button
			doneButton.setInteractive({ useHandCursor: true })
				.on('pointerover', () => doneButton.setStyle({ fill: '#ff0' }))
				.on('pointerout', () => doneButton.setStyle({ fill: '#fff' }))
				.on('pointerdown', () => {
					window.location.href = './sports.html'; // Change window location
				});
		});
	}


	/**
	 * Initializes timers for half-time and second half
	 */
	initTimers() {
		const ticks = scene.gm.timeToTick(state.halfTimeDuration);
		scene.gm.onTick(ticks, () => {
			const isDraw = state.score[0] === state.score[1]
			const postFixText = 'Half Time ⌚\n' + state.teamName[0] + ' vs ' + state.teamName[1] + '\n' + state.score[0] + ' - ' + state.score[1] + '\n' + (isDraw ? "It's A Draw" : (state.score[0] > state.score[1] ? state.teamName[0] + ' Leading!' : state.teamName[1] + ' Leading!'))
			scene.flashHalfTime(postFixText)
		});
		state.lastUpdateTime = scene.time.now
	}


}