import { defineComponent, Types } from 'thirdparty/bitecs'

const Sprite = defineComponent({
	texture: Types.ui8,
	frame: Types.ui16
})

export { Sprite }