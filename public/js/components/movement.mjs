import { defineComponent, Types } from 'thirdparty/bitecs'
import { Vector3 } from 'types'

const Position = defineComponent(Vector3)
const Velocity = defineComponent(Vector3)
const Rotation = defineComponent({
	angle: Types.f32
})


export { Position, Velocity, Rotation }