

import { defineComponent, Types } from 'thirdparty/bitecs'


const Input = defineComponent({
	directionX: Types.i8,
	directionY: Types.i8,
	speed: Types.ui8
})

const UserInput = defineComponent()

const NpcInput = defineComponent()

const NetInput = defineComponent()


export { Input, UserInput, NpcInput, NetInput }