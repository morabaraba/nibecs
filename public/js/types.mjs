import { Types } from 'thirdparty/bitecs'
import { createEnum } from 'utils'

/**
 * @typedef {Object} Vector3
 * @property {number} x - The x-coordinate.
 * @property {number} y - The y-coordinate.
 * @property {number} z - The z-coordinate.
 */
const Vector3 = { x: Types.f32, y: Types.f32, z: Types.f32 }

/**
 * Enum for directions.
 * @readonly
 * @enum {number}
 */
const Direction = {
	None: 0,
	Up: 1,
	Down: 2,
	Left: 3,
	Right: 4,
}

export { Vector3, Direction }
