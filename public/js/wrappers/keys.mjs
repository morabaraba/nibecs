/**
 * @class KeysMouseWrapper
 * Class to handle keyboard and mouse input with pointer locking and centering features.
 */
class KeysMouseWrapper {
	/**
	 * @param {Phaser.Scene} scene - The Phaser scene this wrapper belongs to.
	 * @param {number} idx - The pointer index to track.
	 * @param {boolean} [hideMouse=true] - Whether to hide the mouse cursor.
	 * @param {boolean} [pointerCentering=true] - Whether to enable pointer centering.
	 * @param {number} [recenterThreshold=50] - The threshold for recentering the pointer.
	 */
	constructor(scene, idx, hideMouse = true, pointerCentering = true, recenterThreshold = 50) {
		this.scene = scene
		this.idx = idx
		this.hideMouse = hideMouse
		this.pointerCentering = pointerCentering
		this.recenterThreshold = recenterThreshold
		this.isRecentering = false // Flag for recentering

		// Initialize action states
		this.action1 = { isDown: false }
		this.action2 = { isDown: false }
		this.action3 = { isDown: false }

		// Initialize movement states
		this.up = { isDown: false }
		this.down = { isDown: false }
		this.left = { isDown: false }
		this.right = { isDown: false }

		// Track previous pointer position
		this.previousPointerX = null
		this.previousPointerY = null

		// Set up event listeners
		this.scene.input.on('pointerdown', this.onPointerDown, this)
		this.scene.input.on('pointerup', this.onPointerUp, this)
		this.scene.input.on('pointermove', this.onPointerMove, this)

		// Add event listeners for mouse enter and leave
		this.scene.input.on('gameout', this.onMouseOut, this)
		this.scene.input.on('gameover', this.onMouseIn, this)

		// Call setupPointerLock if hideMouse is enabled
		if (this.hideMouse) {
			this.setupPointerLock()
		}
	}

	/**
	 * Setup pointer lock and hide cursor if required.
	 */
	setupPointerLock() {
		// Enable pointer lock
		this.scene.input.mouse.requestPointerLock()

		// Hide the cursor
		this.scene.input.setDefaultCursor('none')

		// Event listener for pointer lock change
		this.scene.input.on('pointerlockchange', this.pointerLockChange, this)
	}

	/**
	 * Handle pointer lock state changes.
	 */
	pointerLockChange() {
		if (this.scene.input.mouse.locked) {
			console.log('Pointer locked')
		} else {
			console.log('Pointer unlocked')
		}
	}

	/**
	 * Handle pointer down events.
	 * @param {Phaser.Input.Pointer} pointer - The pointer that triggered the event.
	 */
	onPointerDown(pointer) {
		if (pointer.id === this.idx) {
			if (pointer.leftButtonDown()) {
				this.action1.isDown = true
			}
			if (pointer.middleButtonDown()) {
				this.action2.isDown = true
			}
			if (pointer.rightButtonDown()) {
				this.action3.isDown = true
			}
		}
	}

	/**
	 * Handle pointer up events.
	 * @param {Phaser.Input.Pointer} pointer - The pointer that triggered the event.
	 */
	onPointerUp(pointer) {
		if (pointer.id === this.idx) {
			if (pointer.leftButtonReleased()) {
				this.action1.isDown = false
			}
			if (pointer.middleButtonReleased()) {
				this.action2.isDown = false
			}
			if (pointer.rightButtonReleased()) {
				this.action3.isDown = false
			}
		}
	}

	/**
	 * Handle pointer move events.
	 * @param {Phaser.Input.Pointer} pointer - The pointer that triggered the event.
	 */
	onPointerMove(pointer) {
		if (pointer.id === this.idx && !this.isRecentering) {
			// Determine the direction based on pointer movement
			const movementThreshold = 5 // Adjust this value as needed

			if (this.previousPointerX === null || this.previousPointerY === null) {
				// Initialize previous pointer position
				this.previousPointerX = pointer.x
				this.previousPointerY = pointer.y
			}

			const movementX = pointer.x - this.previousPointerX
			const movementY = pointer.y - this.previousPointerY

			// Check if both left and right buttons are down
			if (!(pointer.leftButtonDown() && pointer.rightButtonDown())) {
				this.up.isDown = movementY < -movementThreshold
				this.down.isDown = movementY > movementThreshold
				this.left.isDown = movementX < -movementThreshold
				this.right.isDown = movementX > movementThreshold
			} else {
				// Allow repositioning without movement input
				this.up.isDown = false
				this.down.isDown = false
				this.left.isDown = false
				this.right.isDown = false
			}

			// Update previous pointer position
			this.previousPointerX = pointer.x
			this.previousPointerY = pointer.y

			// Check if the pointer moved outside the recenterThreshold and if pointerCentering is enabled
			if (this.pointerCentering && (Math.abs(movementX) > this.recenterThreshold || Math.abs(movementY) > this.recenterThreshold)) {
				// Set recentering flag
				this.isRecentering = true

				// Recenter the pointer to the center of the screen
				const screenCenterX = this.scene.scale.width / 2
				const screenCenterY = this.scene.scale.height / 2
				this.scene.input.activePointer.position.set(screenCenterX, screenCenterY)

				// Reset the previous pointer position to the center
				this.previousPointerX = screenCenterX
				this.previousPointerY = screenCenterY

				// Clear recentering flag after one frame
				this.scene.time.delayedCall(0, () => {
					this.isRecentering = false
				})
			}
		}
	}

	/**
	 * Handle mouse leaving the canvas.
	 */
	onMouseOut() {
		console.log('Mouse left the canvas')
		// Reset states or perform any cleanup needed
		this.isRecentering = true
		this.previousPointerX = null
		this.previousPointerY = null
		this.up.isDown = false
		this.down.isDown = false
		this.left.isDown = false
		this.right.isDown = false
	}

	/**
	 * Handle mouse entering the canvas.
	 */
	onMouseIn() {
		console.log('Mouse entered the canvas')
		// Reinitialize the previous pointer position
		const screenCenterX = this.scene.scale.width / 2
		const screenCenterY = this.scene.scale.height / 2
		this.previousPointerX = screenCenterX
		this.previousPointerY = screenCenterY

		// Allow normal operations again
		this.isRecentering = false
	}
}

/**
 * @class KeysGamePadWrapper
 * Class to handle gamepad input with action and movement mappings.
 */
class KeysGamePadWrapper extends Phaser.Events.EventEmitter {
	/**
	 * @param {Phaser.Scene} scene - The Phaser scene this wrapper belongs to.
	 * @param {number} idx - The gamepad index to track.
	 */
	constructor(scene, idx) {
		super()
		this.scene = scene
		this.idx = idx

		// Initialize action states
		this.action1 = { isDown: false }
		this.action2 = { isDown: false }
		this.action3 = { isDown: false }

		// Initialize movement states
		this.up = { isDown: false }
		this.down = { isDown: false }
		this.left = { isDown: false }
		this.right = { isDown: false }

		// Initialize gamepad
		this.pad = scene.input.gamepad.getPad(idx)
		if (!this.pad) {
			console.log('Creating gamepad...')
			scene.input.gamepad.once('connected', (pad) => {
				if (pad.index === idx) {
					this.pad = pad
					this.setupListeners()
				}
			})
		} else {
			this.setupListeners()
		}

		// Hook into the scene update event
		this.scene.events.on('update', this.updateAxis, this)
	}

	/**
	 * Setup event listeners for the gamepad.
	 */
	setupListeners() {
		this.pad.on('down', this.onButtonDown, this)
		this.pad.on('up', this.onButtonUp, this)
	}

	/**
	 * Create a mapping of gamepad controls to actions.
	 * @returns {object} The gamepad key mapping.
	 */
	createGamepadMapping() {
		return {
			up: Phaser.Input.Gamepad.Configs.XBOX_360_DPAD_UP,
			down: Phaser.Input.Gamepad.Configs.XBOX_360_DPAD_DOWN,
			left: Phaser.Input.Gamepad.Configs.XBOX_360_DPAD_LEFT,
			right: Phaser.Input.Gamepad.Configs.XBOX_360_DPAD_RIGHT,
			action1: Phaser.Input.Gamepad.Configs.XBOX_360_A,
			action2: Phaser.Input.Gamepad.Configs.XBOX_360_B,
			action3: Phaser.Input.Gamepad.Configs.XBOX_360_X,
		}
	}

	/**
	 * Handle gamepad button down events.
	 * @param {Phaser.Input.Gamepad.Gamepad} pad - The gamepad that triggered the event.
	 * @param {Phaser.Input.Gamepad.Button} button - The button that was pressed.
	 * @param {number} index - The index of the button.
	 */
	onButtonDown(pad, button, index) {
		if (pad.index === this.idx) {
			const keys = this.createGamepadMapping()

			switch (button.index) {
				case keys.up:
					this.up.isDown = true
					break
				case keys.down:
					this.down.isDown = true
					break
				case keys.left:
					this.left.isDown = true
					break
				case keys.right:
					this.right.isDown = true
					break
				case keys.action1:
					this.action1.isDown = true
					break
				case keys.action2:
					this.action2.isDown = true
					break
				case keys.action3:
					this.action3.isDown = true
					break
			}
		}
	}

	/**
	 * Handle gamepad button up events.
	 * @param {Phaser.Input.Gamepad.Gamepad} pad - The gamepad that triggered the event.
	 * @param {Phaser.Input.Gamepad.Button} button - The button that was released.
	 * @param {number} index - The index of the button.
	 */
	onButtonUp(pad, button, index) {
		if (pad.index === this.idx) {
			const keys = this.createGamepadMapping()

			switch (button.index) {
				case keys.up:
					this.up.isDown = false
					break
				case keys.down:
					this.down.isDown = false
					break
				case keys.left:
					this.left.isDown = false
					break
				case keys.right:
					this.right.isDown = false
					break
				case keys.action1:
					this.action1.isDown = false
					break
				case keys.action2:
					this.action2.isDown = false
					break
				case keys.action3:
					this.action3.isDown = false
					break
			}
		}
	}

	/**
	 * Update the movement states based on the gamepad axes.
	 */
	updateAxis() {
		if (this.pad && this.pad.axes.length >= 2) {
			const axisH = this.pad.axes[0].getValue()
			const axisV = this.pad.axes[1].getValue()

			const threshold = 0.1 // Threshold to detect significant axis movement

			this.left.isDown = axisH < -threshold
			this.right.isDown = axisH > threshold
			this.up.isDown = axisV < -threshold
			this.down.isDown = axisV > threshold
		}
	}
}

function createKeyboardMapping(type) {
	const keys = {
		up: null,
		down: null,
		left: null,
		right: null,
		action1: null,
		action2: null,
		action3: null
	}

	switch (type) {
		case '↑←↓→':
			keys.up = Phaser.Input.Keyboard.KeyCodes.UP
			keys.down = Phaser.Input.Keyboard.KeyCodes.DOWN
			keys.left = Phaser.Input.Keyboard.KeyCodes.LEFT
			keys.right = Phaser.Input.Keyboard.KeyCodes.RIGHT
			break
		case 'wasd':
			keys.up = Phaser.Input.Keyboard.KeyCodes.W
			keys.down = Phaser.Input.Keyboard.KeyCodes.S
			keys.left = Phaser.Input.Keyboard.KeyCodes.A
			keys.right = Phaser.Input.Keyboard.KeyCodes.D
			break
		case 'ijkl':
			keys.up = Phaser.Input.Keyboard.KeyCodes.I
			keys.down = Phaser.Input.Keyboard.KeyCodes.K
			keys.left = Phaser.Input.Keyboard.KeyCodes.J
			keys.right = Phaser.Input.Keyboard.KeyCodes.L
			break
		case 'num8426':
			keys.up = Phaser.Input.Keyboard.KeyCodes.NUMPAD_EIGHT
			keys.down = Phaser.Input.Keyboard.KeyCodes.NUMPAD_TWO
			keys.left = Phaser.Input.Keyboard.KeyCodes.NUMPAD_FOUR
			keys.right = Phaser.Input.Keyboard.KeyCodes.NUMPAD_SIX
			break
		case 'num5123':
			keys.up = Phaser.Input.Keyboard.KeyCodes.NUMPAD_FIVE
			keys.down = Phaser.Input.Keyboard.KeyCodes.NUMPAD_TWO
			keys.left = Phaser.Input.Keyboard.KeyCodes.NUMPAD_ONE
			keys.right = Phaser.Input.Keyboard.KeyCodes.NUMPAD_THREE
			break
		case 'num8456':
			keys.up = Phaser.Input.Keyboard.KeyCodes.NUMPAD_EIGHT
			keys.down = Phaser.Input.Keyboard.KeyCodes.NUMPAD_FIVE
			keys.left = Phaser.Input.Keyboard.KeyCodes.NUMPAD_FOUR
			keys.right = Phaser.Input.Keyboard.KeyCodes.NUMPAD_SIX
			break
		default:
			console.error('Invalid movement keys')
			return null
	}

	return keys
}

function createActionMapping(parts, keyCodeMap) {
	const keys = {
		action1: keyCodeMap[parts[2].toLowerCase()],
		action2: keyCodeMap[parts[3].toLowerCase()],
		action3: keyCodeMap[parts[4].toLowerCase()]
	}

	return keys
}

function createGamepadMapping(scene, idx) {
	const wrapper = new KeysGamePadWrapper(scene, idx)
	return {
		up: wrapper.up,
		down: wrapper.down,
		left: wrapper.left,
		right: wrapper.right,
		action1: wrapper.action1,
		action2: wrapper.action2,
		action3: wrapper.action3,
		keysCreated: true,
		name: `gamepad${idx}`
	}
}

function createMouseMapping(scene, idx) {
	const mouseWrapper = new KeysMouseWrapper(scene, idx)
	return {
		up: mouseWrapper.up,
		down: mouseWrapper.down,
		left: mouseWrapper.left,
		right: mouseWrapper.right,
		action1: mouseWrapper.action1,
		action2: mouseWrapper.action2,
		action3: mouseWrapper.action3,
		keysCreated: true,
		name: `mouse${idx}`
	}
}

export { KeysMouseWrapper, KeysGamePadWrapper, createKeyboardMapping, createActionMapping, createGamepadMapping, createMouseMapping }