/** global Phaser */
import GameScene from 'scenes/sports'
import { MqPlugin } from 'plugins/mq'


let game

function createGame(settings) {


	// Game data configuration with the number of players and player speed
	const gameData = { players: 3, playerSpeed: 3, settings: settings }

	/**
	 * @type {Phaser.Types.Core.GameConfig} - Configuration object for the Phaser game instance.
	 */
	const config = {
		backgroundColor: '#1C1C1C',
		type: Phaser.AUTO, // Automatically chooses the best renderer between WebGL and Canvas
		width: 960, // window.innerWidth, // Start with the window width 960
		height: 540, //window.innerHeight, // Start with the window height 540
		physics: {
			default: 'arcade', // Using the arcade physics engine
			arcade: {
				debug: false // Physics debug is turned off
			}
		},
		input: {
			gamepad: true
		},
		scene: [GameScene], // Array of scenes to start with
		disableContextMenu: true, // Disables the right-click context menu
		antialias: false, // Disable anti-aliasing for pixel-perfect rendering
		pixelArt: true, // Ensures a pixel-perfect rendering
		plugins: {
			global: [
				{
					key: 'MqPlugin', // Key for the plugin
					plugin: MqPlugin, // The plugin to use
					start: true, // Start the plugin immediately
					data: gameData // Data to pass to the plugin
				}
			]
		},
		/*fps: {
			min: Number(gameData.settings['game-fps-min'] || 30),
			target: Number(gameData.settings['game-fps-target'] || 120),
			limit:  Number(gameData.settings['game-fps-limit'] || 120*2) // Set your desired FPS limit here
		}, // TODO figure best fps for desktop, mobile, etc */
		parent: 'game'
	}

	// Create a new Phaser game instance with the provided configuration
	game = new Phaser.Game(config)

	// Add fullscreen button
	const fullscreenButton = document.createElement('a')
	fullscreenButton.classList.add('btn')
	fullscreenButton.id = 'fullscreenButton'
	fullscreenButton.textContent = '🖥️'
	document.body.appendChild(fullscreenButton)

	// Add sound toggle button
	const soundButton = document.createElement('a')
	soundButton.classList.add('btn')
	soundButton.id = 'soundButton'
	soundButton.textContent = '🔇'
	document.body.appendChild(soundButton)
	// force game mute
	game.sound.mute = true

	// Fullscreen toggle on button click
	document.getElementById('fullscreenButton').addEventListener('click', () => {
		if (!document.fullscreenElement) {
			requestFullscreen(document.documentElement)
		} else {
			exitFullscreen()
		}
	})

	// Sound toggle on button click
	document.getElementById('soundButton').addEventListener('click', () => {
		if (game.sound.mute) {
			game.sound.mute = false
			soundButton.textContent = '🔊'
		} else {
			game.sound.mute = true
			soundButton.textContent = '🔇'
		}
	})

	// Function to request fullscreen
	function requestFullscreen(element) {
		if (element.requestFullscreen) {
			element.requestFullscreen().catch(err => {
				console.log(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`)
			})
		} else if (element.mozRequestFullScreen) { // Firefox
			element.mozRequestFullScreen().catch(err => {
				console.log(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`)
			})
		} else if (element.webkitRequestFullscreen) { // Chrome, Safari and Opera
			element.webkitRequestFullscreen().catch(err => {
				console.log(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`)
			})
		} else if (element.msRequestFullscreen) { // IE/Edge
			element.msRequestFullscreen().catch(err => {
				console.log(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`)
			})
		}
	}

	// Function to exit fullscreen
	function exitFullscreen() {
		if (document.exitFullscreen) {
			document.exitFullscreen().catch(err => {
				console.log(`Error attempting to exit full-screen mode: ${err.message} (${err.name})`)
			})
		} else if (document.mozCancelFullScreen) { // Firefox
			document.mozCancelFullScreen().catch(err => {
				console.log(`Error attempting to exit full-screen mode: ${err.message} (${err.name})`)
			})
		} else if (document.webkitExitFullscreen) { // Chrome, Safari and Opera
			document.webkitExitFullscreen().catch(err => {
				console.log(`Error attempting to exit full-screen mode: ${err.message} (${err.name})`)
			})
		} else if (document.msExitFullscreen) { // IE/Edge
			document.msExitFullscreen().catch(err => {
				console.log(`Error attempting to exit full-screen mode: ${err.message} (${err.name})`)
			})
		}
	}

	/*
	// Function to resize the game canvas
	function resizeGame() {
		const newWidth = window.innerWidth
		const newHeight = window.innerHeight

		const scaleFactor = Math.min(newWidth / config.width, newHeight / config.height)
		const gameWidth = config.width * scaleFactor
		const gameHeight = config.height * scaleFactor

		const marginTop = (newHeight - gameHeight) / 2

		game.scale.resize(gameWidth, gameHeight)
		game.canvas.style.width = `${gameWidth}px`
		game.canvas.style.height = `${gameHeight}px`
		game.canvas.style.marginTop = `${marginTop}px`
	}

	// Listen for fullscreen change event
	document.addEventListener('fullscreenchange', resizeGame)
	document.addEventListener('mozfullscreenchange', resizeGame)
	document.addEventListener('webkitfullscreenchange', resizeGame)
	document.addEventListener('msfullscreenchange', resizeGame)

	// Resize game when window size changes
	window.addEventListener('resize', resizeGame)

	// Initial resize to fit the current window size
	resizeGame()
	*/

	// Return the game instance inside an array
	return game
}
/**
 * Sets up the game with the specified arguments.
 * @param {object} args - The arguments to configure the game.
 * @returns {Array} - Returns an array containing the game instance.
 */
function setupGame() {
		const result = []
		const db = new PouchDB('game_database');

		function getQueryParam(param) {
			const urlParams = new URLSearchParams(window.location.search);
			return urlParams.get(param);
		}

		async function loadGameData() {
			const docId = getQueryParam('key');
			if (docId) {
				try {
					const settings = await db.get(docId);
					console.debug('game settings from db doc', JSON.stringify(settings));
					result.push(createGame(settings))
				} catch (error) {
					console.error('Error loading document:', error);
				}
			} else {
				console.error('No document ID found in URL.');
			}
		}
		window.addEventListener('load', loadGameData);
	return result
}

export { setupGame }
