/** global Phaser */
import {
	defineSystem,
	defineQuery,
} from 'thirdparty/bitecs'

import { Velocity, Rotation } from 'components/movement'
import { Input, NpcInput } from 'components/input'

// Constants for timers and values
const RUN_DURATION_MIN = 10
const RUN_DURATION_MAX = 15
const WAIT_DURATION_MIN = 10
const WAIT_DURATION_MAX = 15
const DISTANCE_THRESHOLD = 1.5 // Multiplier for screen width
const RANDOM_ACTION_PROBABILITY = 0.1
const SMOOTH_MOVE_PROBABILITY = 0.02

const npcQuery = defineQuery([NpcInput, Velocity, Rotation, Input])

let anyKeyDown = false // to play sound if any npc is moving

/**
 * Update NPC input to move towards the ball with randomness and smoother movements.
 * @param {object} world - The ECS world.
 * @param {number} eid - The entity ID.
 * @param {Phaser.Scene} scene - The Phaser scene.
 */
function updateNpcInput(world, eid, scene) {
	let input = world.p.input
	input.eid = eid
	input.directionX = 0
	input.directionY = 0

	const npc = scene.gm.sprites[eid]
	const ball = scene.ball

	if (!npc || !ball) return

	// Initialize player states if not present
	if (!scene.gm.players[eid].startPosition) {
		scene.gm.players[eid].startPosition = { x: scene.gm.sprites[eid].x, y: scene.gm.sprites[eid].y }
	}
	if (!scene.gm.players[eid].runningAxisDirection) {
		scene.gm.players[eid].runningAxisDirection = 'xy'
	}
	if (!scene.gm.players[eid].runningAxisFor) {
		scene.gm.players[eid].runningAxisFor = 0
	}
	if (!scene.gm.players[eid].runningAxisCounter) {
		scene.gm.players[eid].runningAxisCounter = 0
	}
	if (!scene.gm.players[eid].distanceFromTimer) {
		scene.gm.players[eid].distanceFromTimer = 0
	}
	if (!scene.gm.players[eid].targetPosition) {
		scene.gm.players[eid].targetPosition = { x: ball.x, y: ball.y }
	}
	if (!scene.gm.players[eid].runState) {
		scene.gm.players[eid].runState = { running: false, timer: 0, waitTimer: 0 }
	}

	// Handle random actions
	let keys = scene.gm.players[eid]?.keys || {
		action1: { isDown: false },
		action2: { isDown: false },
		action3: { isDown: false },
	}

	// Check if NPC is waiting
	if (scene.gm.players[eid].waitingCounter < scene.gm.players[eid].waitingFor) {
		scene.gm.players[eid].waitingCounter += scene.time.delta / 1000
		return
	}

	const dx = ball.x - npc.x
	const dy = ball.y - npc.y
	const screenWidthLimit = scene.screenWidth / DISTANCE_THRESHOLD

	const distanceFromStart = Math.sqrt(
		Math.pow(npc.x - scene.gm.players[eid].startPosition.x, 2) +
		Math.pow(npc.y - scene.gm.players[eid].startPosition.y, 2)
	)

	// Force NPC to return to start position if too far or distanceFromTimer triggers
	if (distanceFromStart > screenWidthLimit || scene.gm.players[eid].distanceFromTimer > 0) {
		input.directionX = Math.sign(scene.gm.players[eid].startPosition.x - npc.x)
		input.directionY = Math.sign(scene.gm.players[eid].startPosition.y - npc.y)

		// Choose a random time before NPC needs to check again
		scene.gm.players[eid].runningAxisFor = 0.5 + Math.random() * 3
		scene.gm.players[eid].runningAxisCounter = 0

		// Reset distanceFromTimer after returning to start
		if (distanceFromStart <= screenWidthLimit) {

			scene.gm.players[eid].distanceFromTimer += scene.time.delta / 1000
		} else {
			scene.gm.players[eid].distanceFromTimer = 0
		}

		return
	}

	// Add some randomness to behavior
	const randomFactor = Math.random()
	const performRandomAction = Math.random() < RANDOM_ACTION_PROBABILITY

	if (randomFactor < 0.7) { // 70% chance to follow the ball
		switch (scene.gm.players[eid].runningAxisDirection) {
			case 'x':
				input.directionX = Math.sign(dx)
				input.directionY = 0
				anyKeyDown = true
				break
			case 'y':
				input.directionX = 0
				input.directionY = Math.sign(dy)
				anyKeyDown = true
				break
			case 'xy':
				input.directionX = Math.sign(dx)
				input.directionY = Math.sign(dy)
				anyKeyDown = true
				break
			default:
				input.directionX = 0
				input.directionY = 0
		}
	}

	// Check if the current sprite is overlapping the ball to kick it
	if (scene.physics.overlap(npc, ball)) {
		keys.action2.isDown = true
	} else {
		keys.action2.isDown = false
	}

	// Update target position to follow the ball
	scene.gm.players[eid].targetPosition = { x: ball.x, y: ball.y }

	// Move towards target position
	input.directionX = Math.sign(scene.gm.players[eid].targetPosition.x - npc.x)
	input.directionY = Math.sign(scene.gm.players[eid].targetPosition.y - npc.y)

	// Run state management
	if (scene.gm.players[eid].runState.running) {
		scene.gm.players[eid].runState.timer += scene.time.delta / 1000
		keys.action1.isDown = true

		if (scene.gm.players[eid].runState.timer >= RUN_DURATION_MIN + Math.random() * (RUN_DURATION_MAX - RUN_DURATION_MIN)) {
			scene.gm.players[eid].runState.running = false
			scene.gm.players[eid].runState.waitTimer = WAIT_DURATION_MIN + Math.random() * (WAIT_DURATION_MAX - WAIT_DURATION_MIN)
			scene.gm.players[eid].runState.timer = 0
		}
	} else {
		keys.action1.isDown = false
		scene.gm.players[eid].runState.waitTimer -= scene.time.delta / 1000

		if (scene.gm.players[eid].runState.waitTimer <= 0) {
			scene.gm.players[eid].runState.running = true
		}
	}

	// Update runningAxisCounter
	scene.gm.players[eid].runningAxisCounter += scene.time.delta / 1000

	if (scene.gm.players[eid].runningAxisCounter >= scene.gm.players[eid].runningAxisFor) {
		scene.gm.players[eid].runningAxisDirection = Math.random() < 0.5 ? 'x' : (Math.random() < 0.5 ? 'y' : 'xy')
		scene.gm.players[eid].runningAxisFor = 0.5 + Math.random() * 3
		scene.gm.players[eid].runningAxisCounter = 0
	}

	// Check if NPC should wait
	if (Math.random() < RANDOM_ACTION_PROBABILITY) { // 10% chance to start waiting
		scene.gm.players[eid].waitingFor = 0.5 + Math.random() * 1.5
		scene.gm.players[eid].waitingCounter = 0
	}

	scene.gm.players[eid].keys = keys

	// Smooth out movements
	if (Math.random() < SMOOTH_MOVE_PROBABILITY) { // 20% chance to keep moving in the same direction
		const direction = Math.random() < 0.5 ? 'x' : 'y'
		if (direction === 'x') {
			input.directionX = Math.sign(dx)
			input.directionY = 0
		} else {
			input.directionX = 0
			input.directionY = Math.sign(dy)
		}
	}
}

/**
 * Create a system to update NPC input to follow the ball with randomness.
 * @param {Phaser.Scene} scene - The Phaser scene.
 * @returns {Function} The NPC input system.
 */
function createSoccerNpcSystem(scene) {
	return defineSystem((world) => {
		anyKeyDown = false
		npcQuery(world).forEach(eid => {
			if (!scene.gm.players[eid].waitingFor) {
				scene.gm.players[eid].waitingFor = 0
				scene.gm.players[eid].waitingCounter = 0
			}
			if (!scene.gm.players[eid].startPosition) {
				scene.gm.players[eid].startPosition = { x: scene.gm.sprites[eid].x, y: scene.gm.sprites[eid].y }
			}
			updateNpcInput(world, eid, scene)
		})
		if (anyKeyDown) {
			if (!scene.runningGrassSound.isPlaying) {
				scene.runningGrassSound.play()
			}
		} else {
			if (scene.runningGrassSound.isPlaying) {
				scene.runningGrassSound.stop()
			}
		}
		return world
	})
}
export { createSoccerNpcSystem }
/* the above createSoccerNpcSystem is a mess!
im showing it to you to not give me something similiar back!

lets talk about the goal of the npc and state items it knows

scene.gm.players[eid] = {"eid":3,"uuid":"de661dd5-97d4-4679-a707-87b641c7feb8",
"nick":"66",
"team":"left",
"teamPosition":"goalkeeper",
"leftTeam":true,
"rightTeam":false,
"waitingFor":0,
"waitingCounter":0,
"startPosition":{"x":130,"y":270}}

keys.action1 = run
keys.action2 = kicks
keys.action3 = hold the ball

scene.ball the ball we are chasing either left ro right depending on our team.

So lets setup the waiting state, every 2 to 4 seconds the player must stand still.
otherwise it must capture the ball position and start to move to it.

if it is close to the ball but the ball is to the left of it and its in leftTeam it must hold action3
until it on the right of the ball then action3.isDown False and set action2 and action1 to dribble to the right goal
in reverse for rightTeam

think you can start to build me a system like this?
*/