/** global Phaser */
import {
	defineSystem,
	defineQuery,
	enterQuery,
	exitQuery,
	hasComponent
} from 'thirdparty/bitecs'

import { Position, Velocity, Rotation } from 'components/movement'
import { Sprite } from 'components/entity'
import { Input } from 'components/input'

/** @param {Phaser.Scene} scene */
/** @param {string[]} textures */
function createEntitySystem(scene, textures) {
	const spritesById = new Map() //<number, Phaser.GameObjects.Sprite>()

	const spriteQuery = defineQuery([Sprite]) // Position, Rotation,

	const spriteQueryEnter = enterQuery(spriteQuery)
	const spriteQueryExit = exitQuery(spriteQuery)

	return defineSystem((world) => {
		const entitiesEntered = spriteQueryEnter(world)
		for (let i = 0; i < entitiesEntered.length; ++i)
		{
			const id = entitiesEntered[i]
			const texId = Sprite.texture[id]
			const frame = Sprite.frame[id]
			const texture = textures[texId]
			const sprite = scene.gm.sprites[id]
			/* setup here */
		}

		const entities = spriteQuery(world)
		for (let i = 0; i < entities.length; ++i)
		{
			const id = entities[i]

			const sprite = scene.gm.sprites[id]
			if (!sprite)
			{
				//console.error('Could not find sprite for entity', id)
				continue
			}

			sprite.angle = Rotation.angle[id]
			if (scene.gameData.isHost) {
				if (scene.ball === sprite) {
					continue
				}
			}
			//if (hasComponent(world, Input, sprite.eid)) {
			sprite.x = Position.x[id]
			sprite.y = Position.y[id]
		}

		const entitiesExited = spriteQueryExit(world)
		for (let i = 0; i < entitiesExited.length; ++i)
		{
			const id = entitiesEntered[i]
			scene.gm.destroyEntity(id)
		}

		return world
	})
}

export { createEntitySystem }