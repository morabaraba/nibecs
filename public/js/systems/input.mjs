/** global Phaser */
import {
	defineSystem,
	defineQuery,
} from 'thirdparty/bitecs'

import { Velocity, Rotation } from 'components/movement'
import { Input, UserInput } from 'components/input'

/** @param {Phaser.Types.Input.Keyboard.CursorKeys} cursors */

const userInputQuery = defineQuery([UserInput, Velocity, Rotation, Input])

let playRunSound = false

function updateUserInput(eid, sprite, world, scene) {
	const keys = sprite.state.keys
	let input = world.p.input
	input.eid = eid
	input.directionX = 0
	input.directionY = 0

	let isAnyKeyDown = false

	if (keys.left && keys.left.isDown) {
		input.directionX += -1
		isAnyKeyDown = true
	}
	if (keys.right && keys.right.isDown) {
		input.directionX += 1
		isAnyKeyDown = true
	}
	if (keys.up && keys.up.isDown) {
		input.directionY += -1
		isAnyKeyDown = true
	}
	if (keys.down && keys.down.isDown) {
		input.directionY += 1
		isAnyKeyDown = true
	}
	playRunSound = playRunSound || isAnyKeyDown
}

function createUserInputSystem(scene) {
	return defineSystem((world) => {
		playRunSound = false
		userInputQuery(world).forEach(eid => {
			updateUserInput(eid, scene.gm.players[eid], world, scene)
		})
		scene.playRunSound(playRunSound)
		return world
	})
}
export { createUserInputSystem }