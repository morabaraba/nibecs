import { defineSystem, defineQuery } from 'thirdparty/bitecs';
import { Position, Velocity, Rotation } from 'components/movement';
import { Input } from 'components/input';

/**
 * Creates a movement system that handles entity movement and boundary checking.
 * @param {object} scene - The current scene object.
 * @returns {Function} - A system function that processes entity movements.
 */
function createMovementSystem(scene) {
    // Define a query to select entities with Position, Velocity, Input, and Rotation components
    const movementQuery = defineQuery([Position, Velocity, Input, Rotation]);

    return defineSystem((world) => {
        const entities = movementQuery(world);
        const bounds = scene.bounds;

        // Loop through each entity and process its movement
        for (let i = 0; i < entities.length; ++i) {
            const id = entities[i];
            const sprite  = scene.gm.sprites[id]
            // Retrieve input directions and speed for the entity
            const directionX = Input.directionX[id];
            const directionY = Input.directionY[id];
            const speed = Input.speed[id];
            let r = 30;

            // Set velocity to 0 if there's no input in the respective direction
            if (!directionX) { Velocity.x[id] = 0; }
            if (!directionY) { Velocity.y[id] = 0; }

            // Update velocity based on input
            if (directionX) { Velocity.x[id] = directionX * speed; }
            if (directionY) { Velocity.y[id] = directionY * speed; }

            // Define the lookingDirection variable
            let lookingDirection = "";

            // Update rotation based on direction and set lookingDirection
            if (directionX && directionX < 0) {
                Rotation.angle[id] = 150 + r;
                lookingDirection = "left"; // left
            }
            if (directionX && directionX > 0) {
                Rotation.angle[id] = 30 - r;
                lookingDirection = "right"; // right
            }
            if (directionY && directionY < 0) {
                Rotation.angle[id] = 240 + r;
                lookingDirection = "up"; // up
            }
            if (directionY && directionY > 0) {
                Rotation.angle[id] = 120 - r;
                lookingDirection = "down"; // down
            }

            // Log the direction or use it for further processing
            console.log(`Player is looking ${lookingDirection}`);


            // Update position based on velocity
            Position.x[id] += Velocity.x[id];
            Position.y[id] += Velocity.y[id];

            // Retrieve the sprite dimensions
            const spriteWidth = sprite.playerSprite.width;
            const spriteHeight = sprite.playerSprite.height;
            let halfSize;
            if (lookingDirection == 'left' && 'right') {
                halfSize = spriteHeight / 2
            } else if (lookingDirection == 'up' && 'down') {
                halfSize = spriteWidth / 2
            } else {
                halfSize = spriteHeight / 2
            }

            // Boundary checking using scene bounds
            if (Position.x[id]-halfSize < bounds.left) {
                Position.x[id] = bounds.left+halfSize;
                Velocity.x[id] *= -1;
            } else if (Position.x[id]+halfSize > bounds.right) {
                Position.x[id] = bounds.right-halfSize;
                Velocity.x[id] *= -1;
            }

            if (Position.y[id]-halfSize < bounds.top) {
                Position.y[id] = bounds.top+halfSize;
                Velocity.y[id] *= -1;
            } else if (Position.y[id]+halfSize > bounds.bottom) {
                Position.y[id] = bounds.bottom-halfSize;
                Velocity.y[id] *= -1;
            }

            if (scene.gm.players[id] &&
                    scene.gm.players[id].keys &&
                    scene.gm.players[id].keys.action1.isDown) { // make faster if running
                Position.x[id] += Velocity.x[id] / 2;
                Position.y[id] += Velocity.y[id] / 2;
            }
        }

        return world;
    });
}

export { createMovementSystem };
