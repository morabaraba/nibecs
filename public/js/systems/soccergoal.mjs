import { defineSystem, defineQuery } from 'thirdparty/bitecs'
import { Input } from 'components/input'

/** @param {Phaser.Types.Input.Keyboard.CursorKeys} cursors */

const inputQuery = defineQuery([Input])

function createGoalSystem(scene) {
	return defineSystem((world) => {
		const goalDelay = scene.gm.state.goalDelay

		// Loop through each player and move them to their starting position
		inputQuery(world).forEach(eid => {
			const player = scene.gm.players[eid]
			const teamPosition = player.state.data.teamPosition
			const team = player.state.data.team
			// Use the correct dictionary based on the player's team
			let startAngle = team === 'left' ? 0 : 180
			let startPosition = team === 'left'
				? scene.playerLeftPositions.positions[teamPosition]
				: scene.playerRightPositions.positions[teamPosition]
			if (scene.gm.state.halfTime) { // swap sides over halftime
				startPosition = team === 'left'
					? scene.playerRightPositions.positions[teamPosition]
					: scene.playerLeftPositions.positions[teamPosition]
					startAngle = team === 'left' ? 180 : 0
			}
			// Create a tween to move the player back to the starting position
			scene.tweens.add({
				targets: player,
				x: startPosition[0],
				y: startPosition[1],
				ease: 'Power2', // Easing function
				duration: goalDelay,
				onUpdate: function (tween) {
					// Ensure the player's physics body (if any) moves with the sprite
					if (player.body) {
						player.body.position.set(player.x, player.y)
					}
					// Update player position in Bitecs
					player.p.position.x = player.x
					player.p.position.y = player.y
					const progress = tween.progress
					if (progress >= 0.75) {
						// Final 1/4 of the tween duration
						player.angle = startAngle
					} else {
						// Calculate the angle based on movement direction and set the player's angle
						const angle = Phaser.Math.Angle.Between(player.x, player.y, startPosition[0], startPosition[1])
						player.angle = Phaser.Math.RadToDeg(angle) // Adjust if needed for correct orientation
					}
					player.p.rotation.angle = player.angle
				},
			})
		})
		return world
	})
}

export { createGoalSystem }
