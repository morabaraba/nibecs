/** global Phaser */
import {
    defineSystem,
    defineQuery,
} from 'thirdparty/bitecs'

import { Velocity, Rotation } from 'components/movement'
import { Input, UserInput } from 'components/input'

const userBallQuery = defineQuery([Velocity, Rotation, Input])

/**
 * Update the ball's movement based on user input.
 * @param {Phaser.Types.Input.Keyboard.CursorKeys} keys - The keyboard keys for input.
 * @param {Object} world - The ECS world.
 * @param {number} eid - The entity ID.
 * @param {Phaser.Physics.Arcade.Sprite} ball - The ball sprite.
 * @param {Phaser.Physics.Arcade.Sprite} playerSprite - The player sprite.
 */
function updateBallMovement(keys, world, eid, ball, playerSprite) {
    if (keys && keys.action3.isDown) {
        // Hold the ball
        Velocity.x[ball.eid] = 0
        Velocity.y[ball.eid] = 0
    } else {
        let kickStrength = Input.speed[playerSprite.eid] * 25
        if (keys && keys.action2.isDown) { // KICK
            kickStrength = kickStrength * 1.5  // Boost the kick strength if action key is down
        }
        Velocity.x[ball.eid] = Velocity.x[eid] * kickStrength
        Velocity.y[ball.eid] = Velocity.y[eid] * kickStrength
        Rotation.angle[ball.eid] = Rotation.angle[eid] * kickStrength
    }
}

function playkickSounds(scene) {
    // Check if any kick sound is currently playing
    const isAnyKickPlaying = scene.kicks.some(kick => kick.isPlaying);

    // Check if the ball is moving
    const isBallMoving = scene.ball.body.velocity.x !== 0 || scene.ball.body.velocity.y !== 0;

    // Only play a random kick sound if none are playing and the ball is moving
    if (!isAnyKickPlaying && isBallMoving) {
        const randomIndex = Phaser.Math.Between(0, scene.kicks.length - 1);
        scene.kicks[randomIndex].play();
    }
}


/**
 * Creates a system to handle user interaction with the ball.
 * @param {Phaser.Scene} scene - The Phaser scene.
 * @returns {Function} The system function.
 */
function createUserBallSystem(scene) {
    return defineSystem((world) => {
        let interactPlayers = []

        userBallQuery(world).forEach(eid => {
            const playerSprite = scene.gm.sprites[eid]
            if (!playerSprite || !scene.ball) return

            if (Phaser.Geom.Intersects.RectangleToRectangle(playerSprite.getBounds(), scene.ball.getBounds())) {
                interactPlayers.push(eid)
            }
        })

        let actionPlayers = interactPlayers.filter(eid => {
            const keys = scene.gm.players[eid].keys
            return keys && (keys.action2.isDown || keys.action3.isDown) // player that want to interact with ball using kick or hold gets the ball
        })

        let selectedEid
        if (actionPlayers.length > 0) {
            selectedEid = actionPlayers[Math.floor(Math.random() * actionPlayers.length)]
        } else if (interactPlayers.length > 0) {
            selectedEid = interactPlayers[Math.floor(Math.random() * interactPlayers.length)]
        }

        if (selectedEid !== undefined) {
            const playerSprite = scene.gm.sprites[selectedEid]
            const keys = scene.gm.players[selectedEid].keys
            //if (keys) {
                updateBallMovement(keys, world, selectedEid, scene.ball, playerSprite)
                playkickSounds(scene)
            //}
        }

        return world
    })
}

export { createUserBallSystem }
