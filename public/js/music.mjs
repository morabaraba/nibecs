const audioContext = new (window.AudioContext || window.webkitAudioContext)()
const gainNode = audioContext.createGain()
gainNode.connect(audioContext.destination)

let currentTrack
let musicCredits = []
let currentTrackIndex
let previousTrackIndex = -1

const soundButton = document.getElementById('soundButton')
let grow = true
let animationInterval
let isMuted = true


// Function to fetch and load credits
async function fetchCredits() {
	try {
		const response = await fetch('assets/music/song2/credits.json')
		musicCredits = await response.json()
		playRandomTrack()
	} catch (error) {
		console.error('Error fetching credits:', error)
	}
}

// Function to play a random track
async function playRandomTrack() {
	do {
		currentTrackIndex = Math.floor(Math.random() * musicCredits.length)
	} while (currentTrackIndex === previousTrackIndex)

	previousTrackIndex = currentTrackIndex
	const selectedCredit = musicCredits[currentTrackIndex]
	const audioBuffer = await fetchAndDecodeAudio(selectedCredit.mp3)

	if (currentTrack) {
		currentTrack.stop()
	}

	currentTrack = audioContext.createBufferSource()
	currentTrack.buffer = audioBuffer
	currentTrack.connect(gainNode)  // Connect to gain node
	currentTrack.start()

	updateMusicCreditUI(selectedCredit)

	// Update hidden input fields
	document.querySelector('input[name="game-music"]').value = selectedCredit.mp3

	// Start a timer to update the current time position every second
	setInterval(() => {
		if (currentTrack && audioContext.state === 'running') {
			document.querySelector('input[name="game-music-time"]').value = audioContext.currentTime
		}
	}, 1000)

	currentTrack.onended = playRandomTrack
}

// Function to fetch and decode audio
async function fetchAndDecodeAudio(url) {
	const response = await fetch(`assets/music/song2/${url}`)
	const arrayBuffer = await response.arrayBuffer()
	return audioContext.decodeAudioData(arrayBuffer)
}

// Function to update music credit UI
function updateMusicCreditUI(credit) {
	const container = document.getElementById('game')
	const titles = credit.title.split('-')
	container.innerHTML = `
        <span class="js-music-credit">
            <span class="credit-title">Music.........: <a href="${credit.source}">${titles[0]}<br>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${titles[1]}</a></span>
            <img class="credit-artist" src="assets/music/song2/${credit.image}" alt="${credit.title}">
        </span>
    `

	const soundButton = document.getElementById('soundButton')
	soundButton.style.display = 'block' // Make the button visible
	toggleButton(soundButton.textContent == '🔇' ? 1 : 0 )
}

// Fetch credits and start playback
fetchCredits()


function tweenButton(isMuted) {
	if (animationInterval) {
		clearInterval(animationInterval)
	}
	if (isMuted) {
		// Start the grow/shrink animation
		animationInterval = setInterval(() => {
			soundButton.style.transform = grow ? 'scale(1.1)' : 'scale(1)'
			grow = !grow
		}, 500)
	} else {
		// Stop the grow/shrink animation
		clearInterval(animationInterval)
		soundButton.style.transform = 'scale(1)'
	}
}
function toggleButton(mute) {
	if (audioContext.state === 'suspended') {
		audioContext.resume()
		if (audioContext.state === 'suspended') {
			mute = true
		}
	}
	gainNode.gain.value = mute ? 0 : 1
	soundButton.textContent = mute ? '🔇' : '🔊'
	tweenButton(mute)
	isMuted = mute
}

soundButton.addEventListener('click', () => toggleButton(!isMuted))


// Handle visibility change to resume audio context
document.addEventListener('visibilitychange', () => {
	if (document.visibilityState === 'visible' && audioContext.state === 'suspended') {
		audioContext.resume()
	}
})
