/** global Phaser */
import GameScene from 'scenes/game'
import { EcsPlugin } from 'ecsplugin'


/**
 * Sets up two Phaser games with the specified configuration.
 *
 * @returns {Array} An array containing the two Phaser game instances.
 */
function setupGame() {
	let game1, game2

	/** @type {Phaser.Types.Core.GameConfig} */
	const config = {
		type: Phaser.AUTO,
		width: 800,
		height: 600,
		physics: {
			default: 'arcade',
			arcade: {
				gravity: { y: 200 }
			}
		},
		scene: [GameScene],
		disableContextMenu: true,
		plugins: {
			global: [
				{ key: 'EcsPlugin', plugin: EcsPlugin, start: true, data: { game1: null, game2: null } }
			]
		}
	}

	// Configure the first game instance
	config.parent = 'game1'
	game1 = new Phaser.Game(config)

	// Configure the second game instance
	config.parent = 'game2'
	game2 = new Phaser.Game(config)

	// Link the game instances to the plugin data
	config.plugins.global[0].data.game1 = game1
	config.plugins.global[0].data.game2 = game2

	return [game1, game2]
}

export { setupGame }
