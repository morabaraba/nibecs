
// Example usage for 12 players
const settings = []
const playerSettingsContainer = document.getElementById('js-player-settings')
const player0SettingsContainer = document.getElementById('js-player0-settings')
playerSettingsContainer.parentElement.style.display = 'none'

function createSelect(options, width = '290px') {
	const select = document.createElement('select')
	select.className = 'tui-input'
	select.style.width = width
	options.forEach(option => {
		const opt = document.createElement('option')
		opt.value = option
		opt.textContent = option
		select.appendChild(opt)
	})
	return select
}

function generateUUID() {
	// Generate a random 16-digit UUID string
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8)
		return v.toString(16)
	})
}

function createPlayerSettings(idx, nick, options, defaultSelectedIndex) {
	const container = document.createElement('div')
	container.setAttribute('style', 'display: inline-block')
	container.defaultSelectedIndex = defaultSelectedIndex // Save defaultSelectedIndex on the container

	const label = document.createElement('label')
	label.innerHTML = `Player # ${String(idx + 1).padStart(2, '0')}....: `

	const nicknameInput = document.createElement('input')
	nicknameInput.name = `player-${idx}-nick`
	nicknameInput.value = nick
	nicknameInput.setAttribute('maxlength', '6')
	nicknameInput.className = 'tui-input'
	nicknameInput.style.width = '70px'

	const playerKeysSelect = createSelect(options)
	playerKeysSelect.name = `player-${idx}-keys`
	playerKeysSelect.selectedIndex = defaultSelectedIndex + 1

	const playerTeamSelect = createSelect(['| left', '| right'], '88px')
	playerTeamSelect.name = `player-${idx}-team`
	playerTeamSelect.selectedIndex = idx >= 6 ? 1 : 0

	const playerTeamRoleSelect = createSelect(['| forward 1',
		'| forward 2',
		'| midfielder',
		'| defender 1',
		'| defender 2',
		'| goalkeeper',], '88px')
	playerTeamRoleSelect.name = `player-${idx}-role`
	playerTeamRoleSelect.selectedIndex = idx >= 6 ? idx - 6 : idx  //keysOptions[(i - 1) % keysOptions.length]) //idx > 2 ? idx - 3 : idx - 1

	const spriteCount = 14 + 12
	const spriteOptions = []
	for (let i = 0; i <= spriteCount; i++) {
		spriteOptions.push(`| ${i}`)
	}

	const playerSprite = createSelect(spriteOptions, '96px')
	playerSprite.name = `player-${idx}-sprite`
	playerSprite.selectedIndex = Math.floor(Math.random() * (spriteCount + 1))


	if (idx == 0) {

	} else {
		container.appendChild(label)
	}
	container.appendChild(nicknameInput)
	container.appendChild(playerTeamSelect)
	container.appendChild(playerTeamRoleSelect)
	container.appendChild(playerSprite)
	container.appendChild(playerKeysSelect)
	container.appendChild(document.createElement('br'))

	return container
}

function togglePlayerSettings(show) {

	const playerSettings = settings //[playerSettings1, playerSettings2, playerSettings3, playerSettings4]

	if (show) {
		playerSettings.forEach((setting, idx) => {
			if (idx == 0) {
				player0SettingsContainer.appendChild(setting)
			} else {
				playerSettingsContainer.appendChild(setting)
			}

			const selectElement = setting.querySelector(`select[name=player-${idx}-keys]`)
			if (selectElement) {
				selectElement.selectedIndex = setting.defaultSelectedIndex
			}
		})
	} else {
		playerSettings.forEach((setting, idx) => {
			const selectElement = setting.querySelector(`select[name=player-${idx}-keys]`)
			if (selectElement) {
				selectElement.selectedIndex = playerKeysOptions.indexOf('| disabled')
			}
		})
	}
}

const gameNameInput = document.getElementById('js-game-name')
const gamePassInput = document.getElementById('js-game-pass')

// Initially generate game name and pass
gameNameInput.value = gameNameInput.value + generateUUID().split('-')[1]
gamePassInput.value = gamePassInput.value + generateUUID().split('-')[1]

const player0DefaultKeys = "| ↑←↓→ | , \u2003 | . \u2003\u2003 | /"
const player1DefaultKeys = "| ↑←↓→ | ins | home | pgup"
const player2DefaultKeys = "| num5123 | num7 | num8 | num9"
const player3DefaultKeys = "| wasd | 1 | 2 | 3"
const player4DefaultKeys = "| ijkl | 7 | 8 | 9"
const fourCoOpKeys = [player1DefaultKeys, player2DefaultKeys, player3DefaultKeys, player4DefaultKeys]
const npcKeys = '| npc'
const disableKeys = "| disabled"
const networkKeys = '| network'
const playerKeysOptions = [
	player0DefaultKeys,
	npcKeys,
	networkKeys,
	disableKeys,
	player1DefaultKeys,
	"| ↑←↓→ | del | end | pgdown",
	"| ↑←↓→ | q | w | e",
	"| ↑←↓→ | 1 | 2 | 3",
	"| wasd | del | end | pgdown",
	"| wasd | ins | home | pgup",
	"| wasd | i | o | p",
	player3DefaultKeys,
	"| ijkl | q | w | e",
	"| ijkl | 1 | 2 | 3",
	player4DefaultKeys,
	"| num8426 | q | w | e",
	"| num8426 | 1 | 2 | 3",
	"| num8426 | ins | del | enter",
	"| num5123 | q | w | e",
	"| num5123 | 1 | 2 | 3",
	player2DefaultKeys,
	'| mouse 1 | btn left | btn right | btn middle',
	'| gamepad 1 | A | B | X',
	'| mouse 2 | btn left | btn right | btn middle',
	'| mouse 3 | btn left | btn right | btn middle',
	'| mouse 4 | btn left | btn right | btn middle',
	// TODO get the numpad ins del enter working
	//"| num5123 | ins | del | enter",
	//"| num8456 | ins | del | enter",
]

function playerSettings(idx, keysOption) {
	return createPlayerSettings(idx, `P${String(idx + 1).padStart(2, '0')}`, playerKeysOptions, playerKeysOptions.indexOf(keysOption))
}


for (let i = 0; i < 12; i++) {
	settings.push(playerSettings(i, i < 1 ? playerKeysOptions[i] : '| npc')) //: playerKeysOptions[(i ) % playerKeysOptions.length]))
}


togglePlayerSettings(true)


// Handle game type change
document.getElementById('game-type').addEventListener('change', (event) => {
	if (event.target.value === 'join') {
		gameNameInput.value = ''
		gamePassInput.value = ''
		togglePlayerSettings(false)
	} else {
		gameNameInput.value = 'demo' + generateUUID().split('-')[1]
		gamePassInput.value = 'pass' + generateUUID().split('-')[1]
		togglePlayerSettings(true)
	}
})


const db = new PouchDB('game_database')

document.getElementById('gameForm').addEventListener('submit', async (event) => {
	event.preventDefault()

	const formData = new FormData(event.target)
	const data = {}
	formData.forEach((value, key) => {
		data[key] = value
	})

	try {
		const result = await db.put({
			_id: new Date().toISOString(),
			...data
		})

		const docId = result.id
		window.location.href = `index.html?game=sports&key=${docId}`
	} catch (error) {
		console.error('Error storing document:', error)
	}
})
const buttons = document.querySelectorAll('a[id^="js-"]')
buttons.forEach(button => {
	button.addEventListener('click', (event) => {
		const buttonId = event.target.id
		const selectElements = document.querySelectorAll('select[name^="player-"][name$="-keys"]')
		switch (buttonId) {
			case 'js-1-player':
				console.debug('1 Player button clicked:', button)
				selectElements.forEach((select, idx) => {
					select.value = idx === 0 ? player0DefaultKeys : npcKeys
				})
				break
			case 'js-4-co-op':
				console.debug('4 Co-Op button clicked:', button)
				selectElements.forEach((select, idx) => {
					if (idx >= 0 && idx < 4) {
						select.value = fourCoOpKeys[idx]
					} else {
						select.value = npcKeys
					}
				})
				break
			case 'js-disable-all':
				console.debug('Disable All button clicked:', button)
				selectElements.forEach((select, idx) => {
					select.value = idx === 0 ? player0DefaultKeys : disableKeys
				})
				break
			case 'js-network-all':
				console.debug('Network All button clicked:', button)
				selectElements.forEach((select, idx) => {
					select.value = idx === 0 ? player0DefaultKeys : networkKeys
				})
				break
			case 'js-npc-all':
				console.debug('Npc All button clicked:', button)
				selectElements.forEach((select) => {
					select.value = npcKeys
				})
				break
			case 'js-settings':
				console.debug('Settings button clicked:', button)
				if (playerSettingsContainer.parentElement.style.display === 'none') {
					playerSettingsContainer.parentElement.style.display = 'block'
				} else {
					playerSettingsContainer.parentElement.style.display = 'none'
				}
				break
			default:
				console.debug('Unknown button clicked:', button)
		}
	})
});
/*});*/

