/** global Phaser */

import {
	createWorld,
	addEntity,
	addComponent,
	defineSerializer,
	defineDeserializer,
	DESERIALIZE_MODE
} from 'thirdparty/bitecs'

import { Input, UserInput } from 'components/input'

import { createMovementSystem } from 'systems/movement'
import { createUserInputSystem } from 'systems/input'
import { createEntitySystem } from 'systems/entity'

import { GameManagerPlugin } from 'plugins/gamemanager'

const planets = [
	['name','texture','object', 'radius','time'],
	['sun', 'images/tiny-planet/Sun-128x128.png', { frameWidth: 128, frameHeight: 128 }, 0,  0],
	['earth', 'images/tiny-planet/Earth-128x128.png', { frameWidth: 128, frameHeight: 128 }, 200, 0.001],
	['moon', 'images/tiny-planet/Moon-128x128.png', { frameWidth: 128, frameHeight: 128 }, 40, 0.0002],
	['mercury', 'images/tiny-planet/Mercury-128x128.png', { frameWidth: 128, frameHeight: 128 }, 100, 0.0008],
	['venus', 'images/tiny-planet/Venus-128x128.png', { frameWidth: 128, frameHeight: 128 }, 150, 0.0006],
	['mars', 'images/tiny-planet/Mars-128x128.png', { frameWidth: 128, frameHeight: 128 }, 250, 0.0004],
	['jupiter', 'images/tiny-planet/Jupiter-128x128.png', { frameWidth: 128, frameHeight: 128 }, 350,  0.0002],
	['saturn', 'images/tiny-planet/Saturn-128x128.png', { frameWidth: 128, frameHeight: 128 }, 450,  0.0001],
	['uranus', 'images/tiny-planet/Uranus-128x128.png', { frameWidth: 128, frameHeight: 128 }, 550,  0.00008],
	['neptune', 'images/tiny-planet/Neptune-128x128.png', { frameWidth: 128, frameHeight: 128 }, 650, 0.00006],
]
const orbitRadius = {}
const orbitTimes = {}

class SolarSystemScene extends Phaser.Scene {
	init() {
		//console.log('SolarSystemScene init.')
	}
	preload() {
		let scene = this
		planets.forEach((v, idx) => {
			if (idx == 0 ) return
			//console.log('SolarSystemScene Preload: ' + JSON.stringify(v))
			scene.load.spritesheet(v[0],v[1],v[2])
			orbitRadius[v[0]] = v[3]
			orbitTimes[v[0]] = v[4]
		})
	}
	create() {
		this.plugins.installScenePlugin('GameManagerPlugin', GameManagerPlugin, 'gm', this);
		const scene = this
		let ecs = scene.ecs = scene.plugins.get('EcsPlugin');
		let world = scene.world = scene.gm.world//this.ecs.newWorld()
		// screen width and height for easy access
		const screenWidth = scene.screenWidth = scene.cameras.main.width;
		const screenHeight = scene.screenHeight = scene.cameras.main.height;
		world.p = scene.gm.createProxies()
		scene.planet_eids = {}
		planets.forEach((v,idx) => {
			if (idx == 0 ) return
			//console.log('SolarSystemScene Create: ' + JSON.stringify(v))
			let name = v[0]
			scene.planet_eids[name] = scene.gm.addSprite( name )
			world.p.sprite.eid = scene.planet_eids[name]
			world.p.sprite.texture = scene.texture_keys().indexOf(name)
			world.p.sprite.frame = 5
			//scene.load.spritesheet(v[0],v[1],v[2])
		})
		world.p.position.eid = scene.planet_eids['sun']
		world.p.position.x = screenWidth / 2
		world.p.position.y = screenHeight / 2
		scene.entitySystem = createEntitySystem(scene, scene.texture_keys())
		scene.movementSystem = createMovementSystem(scene)
		scene.serialize = defineSerializer(world)
		scene.deserialize = defineDeserializer(world)
	}
	update(time, delta) {
		//this.userInputSystem(this.world)
		this.entitySystem(this.world)
		this.movementSystem(this.world)
		let world = this.world
		world.p.position.eid = this.planet_eids['sun']
		let sunX = world.p.position.x
		let sunY = world.p.position.y
		world.p.position.eid = this.planet_eids['earth']
		let earthX = world.p.position.x
		let earthY = world.p.position.y
		planets.forEach((v,idx) => {
			if (idx == 0 ) return
			let name = v[0]
			if (name == 'sun' ) return
			world.p.position.eid = this.planet_eids[name]
			if (name == 'moon' ) {
				world.p.position.x = earthX + orbitRadius[name] * Math.cos(time * orbitTimes[name]); // Adjust speed
				world.p.position.y = earthY + orbitRadius[name] * Math.sin(time * orbitTimes[name]);
			} else {
				world.p.position.x = sunX + orbitRadius[name] * Math.cos(time * orbitTimes[name]); // Adjust speed
				world.p.position.y = sunY + orbitRadius[name] * Math.sin(time * orbitTimes[name]);
			}
			//earth._atmosphere.setPosition(earthX, earthY);
		})
	}
	texture_keys() {
		return Object.keys(this.textures.list)
	}
	serializeWorld() {
		const packet = this.serialize(this.world)
		return packet
	}
	deserializeWorld(packet) {
		if (!this.deserialize) return
		const mode = DESERIALIZE_MODE.MAP
		this.deserializedEnts = this.deserialize(this.world, packet, mode)
		return this.deserializedEnts
	}
}

export { SolarSystemScene }