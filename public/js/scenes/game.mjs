/** global Phaser */

import {
	createWorld,
	addEntity,
	addComponent,
	defineSerializer,
	defineDeserializer,
	DESERIALIZE_MODE
} from 'thirdparty/bitecs'
import { InputProxy, PositionProxy, VelocityProxy, SpriteProxy } from 'proxies'
import { Position, Velocity, Rotation } from 'components/movement'
import { Input, UserInput } from 'components/input'
import { Sprite } from 'components/entity'
import { createMovementSystem } from 'systems/movement'
import { timeSystem } from 'systems/time'
import { createUserInputSystem } from 'systems/input'
import { createEntitySystem } from 'systems/entity'

const speed = 10

export default class GameScene extends Phaser.Scene {

	init() {
		//console.log('GameScene init.')
	}

	preload() {
		this.load.image("block", "/examples/public/assets/sprites/block.png");
	}

	create() {
		this.plugins.installScenePlugin('GameManagerPlugin', GameManagerPlugin, 'gm', this);
		this.ecs = this.plugins.get('EcsPlugin');
		// create this.world
		this.world = createWorld()
		// setup proxies
		this.world.input = new InputProxy(0)
		this.world.position = new PositionProxy(0)
		this.world.sprite = new SpriteProxy(0)
		// create player eid and add components
		this.peid = addEntity(this.world)
		addComponent(this.world, Position, this.peid)
		addComponent(this.world, Velocity, this.peid)
		addComponent(this.world, Rotation, this.peid)
		addComponent(this.world, Sprite, this.peid)
		addComponent(this.world, UserInput, this.peid)
		addComponent(this.world, Input, this.peid)

		this.world.position.eid = this.peid
		this.world.position.x = 100
		this.world.position.y = 100

		this.world.sprite.eid = this.peid
		this.world.sprite.texture = 0

		this.world.input.eid = this.peid
		this.world.input.speed = speed

		// get this.keys and setup userInput System
		this.keys = this.input.keyboard.addKeys({ up: 'W', left: 'A', down: 'S', right: 'D' });
		this.userInputSystem = createUserInputSystem(this.keys)
		this.entitySystem = createEntitySystem(this, ['block'])
		this.movementSystem = createMovementSystem()

		this.text = this.add.text(20, 40, '', { color: '#fff', stroke: '#fff', });

		this.serialize = defineSerializer(this.world)
		this.deserialize = defineDeserializer(this.world)
		/*
		// create player eid and add components

		addComponent(this.world, UserInput, this.peid)
		addComponent(this.world, Input, this.peid)

		this.world.position.eid = this.peid
		this.world.position.x = 100
		this.world.position.y = 100

		this.world.sprite.eid = this.peid
		this.world.sprite.texture = 0

		this.world.input.eid = this.peid
		this.world.input.speed = speed

		// get this.keys and setup userInput System
		this.keys = this.input.keyboard.addKeys({ up: 'W', left: 'A', down: 'S', right: 'D' });
		this.userInputSystem = createUserInputSystem(this.keys)
		*/
	}

	serializeWorld() {
		if (!this.serialize) return
		this.world.position.x = this.world.position.x + 1
		if (this.world.position.x > 200)
			this.world.position.x = 100
		const packet = this.serialize(this.world)
		return packet
	}
	deserializeWorld(packet) {
		if (!this.deserialize) return
		const mode = DESERIALIZE_MODE.MAP
		this.deserializedEnts = this.deserialize(this.world, packet, mode)
		return this.deserializedEnts
	}
	update() {
		this.entitySystem(this.world)
		this.movementSystem(this.world)
		if (this.game.config.parent == 'game1') {
			this.userInputSystem(this.world)

			this.ecs.updateGame2()
		}
		/*this.text.text(
			Object.entries(this.keys).map(([name, key]) => `${name}: keyCode=${key.keyCode} isDown=${key.isDown} isUp=${key.isUp} timeDown=${key.timeDown} timeUp=${key.timeUp}`)
		);*/
	}
}

/*
import Sprite from 'components/Sprite'
import Rotation from 'components/Rotation'
import CPU from 'components/CPU'
import Input from 'components/Input'

import createMovementSystemfrom 'systems/movement'
import createSpriteSystem from 'systems/sprite'
import createPlayerSystem from 'systems/player'
import createCPUSystem from 'systems/cpu'

enum this.textures
{
	TankBlue,
	TankGreen,
	TankRed
}

export default class Game extends Phaser.Scene
{
	private cursors!: Phaser.Types.Input.Keyboard.Cursorthis.keys

	private this.world!: Ithis.world
	private playerSystem!: System
	private cpuSystem!: System
	private this.movementSystem!: System
	private spriteSystem!: System

	constructor()
	{
		super('game')
	}

	init()
	{
		this.cursors = this.input.keyboard.createCursorthis.keys()
	}

	preload()
	{
		this.load.image('tank-blue', 'assets/tank_blue.png')
		this.load.image('tank-green', 'assets/tank_green.png')
		this.load.image('tank-red', 'assets/tank_red.png')
	}

	create()
	{
		const { width, height } = this.scale

		this.this.world = createWorld()

		// create the player tank
		const blueTank = addEntity(this.this.world)

		addComponent(this.this.world, Position, blueTank)
		addComponent(this.this.world, Velocity, blueTank)
		addComponent(this.this.world, Rotation, blueTank)
		addComponent(this.this.world, Sprite, blueTank)
		addComponent(this.this.world, Player, blueTank)
		addComponent(this.this.world, Input, blueTank)

		Position.x[blueTank] = 100
		Position.y[blueTank] = 100
		Sprite.this.texture[blueTank] = this.textures.TankBlue
		Input.speed[blueTank] = 10

		// create random cpu tanks
		for (let i = 0; i < 10; ++i)
		{
			const tank = addEntity(this.this.world)

			addComponent(this.this.world, Position, tank)
			Position.x[tank] = Phaser.Math.Between(width * 0.25, width * 0.75)
			Position.y[tank] = Phaser.Math.Between(height * 0.25, height * 0.75)

			addComponent(this.this.world, Velocity, tank)
			addComponent(this.this.world, Rotation, tank)

			addComponent(this.this.world, Sprite, tank)
			Sprite.this.texture[tank] = Phaser.Math.Between(1, 2)

			addComponent(this.this.world, CPU, tank)
			CPU.timeBetweenActions[tank] = Phaser.Math.Between(0, 500)

			addComponent(this.this.world, Input, tank)
			Input.speed[tank] = 10
		}

		// create the systems
		this.playerSystem = createPlayerSystem(this.cursors)
		this.cpuSystem = createCPUSystem(this)
		this.this.movementSystem = createthis.movementSystem()
		this.entitySystem = createEntitySystem(this, ['tank-blue', 'tank-green', 'tank-red'])
	}

	update(t: number, dt: number) {
		// run each system in desired order
		this.playerSystem(this.this.world)
		this.cpuSystem(this.this.world)

		this.this.movementSystem(this.this.world)

		this.spriteSystem(this.this.world)
	}
}
*/