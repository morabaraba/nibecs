/** global Phaser */
// bitECS Entity Component System
import {
	defineSerializer,
	defineDeserializer,
} from 'thirdparty/bitecs'
// ECS Systems
import { createUserBallSystem } from 'systems/ball'
import { createMovementSystem } from 'systems/movement'
import { createUserInputSystem } from 'systems/input'
import { createEntitySystem } from 'systems/entity'
import { createSoccerNpcSystem } from 'systems/soccernpc'
import { createGoalSystem } from 'systems/soccergoal'

// Phaser Plugins
import { GameManagerPlugin } from 'plugins/gamemanager'
import { MusicPlugin } from 'plugins/music'

import { NpcManagerPlugin } from 'plugins/npcmanager'
// Utils etc
import { applyMixins } from 'utils'

import SportsSceneMixin from 'mixins/scenes/sports'

/** @type {Phaser.Scene} */
let scene // Forward declaration of scene to get rid of scene.

/**
 * @class BaseGameScene
 * @extends Phaser.Scene
 * A base scene for the game, handling initialization and preloading of assets.
 */
class BaseGameScene extends Phaser.Scene {

	/**
	 * Initializes the game scene.
	 * Sets the global reference to the scene.
	 */
	init() {
		scene = this
		console.debug('GameScene init.', scene)
	}

	/**
	 * Preloads game assets including textures and sounds.
	 */
	preload() {
		// Load textures
		scene.load.image("ball", "assets/textures/soccer-ball.png")
		scene.load.spritesheet('characters', 'assets/textures/sports-players.png', { frameWidth: 21, frameHeight: 31 })

		// Load sound effects
		scene.load.audio('goal', 'assets/sounds/goal.mp3')
		scene.load.audio('hitPost', 'assets/sounds/hit-post.mp3')
		scene.load.audio('kick1', 'assets/sounds/kick-1.mp3')
		scene.load.audio('kick2', 'assets/sounds/kick-2.mp3')
		scene.load.audio('kick3', 'assets/sounds/kick-3.mp3')
		scene.load.audio('runningGrass', 'assets/sounds/running-grass.mp3')
	}

	/**
	 * Initializes the game scene, setting up plugins, game data, state, textures, field, goals, players, sounds, and systems.
	 */
	create() {
		// Install GameManagerPlugin for the scene
		scene.plugins.installScenePlugin('GameManagerPlugin', GameManagerPlugin, 'gm', scene)
		scene.plugins.installScenePlugin('MusicPlugin', MusicPlugin, 'music', scene)

		// scene.plugins.installScenePlugin('NpcManagerPlugin', NpcManagerPlugin, 'npc', scene); // Uncomment if needed

		// Initialize npc object for using the soccer npc system for now
		scene.npc = {}

		// Get message queue plugin
		scene.mq = scene.plugins.get('MqPlugin')

		// Call the superMixins method with the arguments
		scene.superMixins(arguments)

		// Set up game data and game state
		scene.gameData = scene.mq.data // Game data setup and config
		scene.gameState = scene.gm.state // Game state to remember state during the game
		scene.gameState.score = [0, 0] // Initialize score [left, right]

		// Allow mq to access the scene
		scene.mq.scene = scene

		// Hook up game manager to process messages from the host
		scene.mq.hostMessageArrive = scene.gm.hostMessageArrive

		// Create Mixin Stuff
		scene.createMixin()
		// Get texture keys for Sprite Entity to share texture info in bitECS
		scene.textureNames = scene.textures.getTextureKeys()

		// Set some npc sprite sheet and frame info
		scene.spriteSheetName = 'characters'

		// Get screen width and height for easy access
		const screenWidth = scene.screenWidth = scene.cameras.main.width
		const screenHeight = scene.screenHeight = scene.cameras.main.height

		// Setup play area with field and goals
		scene.createSoccerField()
		scene.createSoccerGoal(3, (screenHeight / 2) - 25, 270)
		scene.createSoccerGoal(screenWidth - 94, (screenHeight / 2) - 25, 90)

		// Create ball sprite
		scene.createBall()

		// Create position placements for teams
		scene.playerLeftPositions = scene.createPlayerStartPosition('left')
		scene.playerRightPositions = scene.createPlayerStartPosition('right')

		// Check which players are enabled
		scene.createTeamPlayers()

		// Create sound objects in the scene
		scene.createSound()
		scene.music.start()
		// Setup bitECS systems
		scene.entitySystem = createEntitySystem(scene, scene.textureNames)
		scene.userInputSystem = createUserInputSystem(scene)
		scene.movementSystem = createMovementSystem(scene)
		scene.ballSystem = createUserBallSystem(scene)
		scene.npcSystem = createSoccerNpcSystem(scene)
		scene.goalSystem = createGoalSystem(scene)



		// To sync bitECS world via mq
		scene.serialize = defineSerializer(scene.gm.world)
		scene.deserialize = defineDeserializer(scene.gm.world)

		console.debug('GameScene Created.', scene)
		scene.showTimerText();
		scene.initTimers();
	}

	/**
	 * Updates the game scene, processing user inputs, ball movements, NPC behaviors,
	 * entity systems, and movement systems. Also updates the scoreboard.
	 */
	render() {
		if (scene.gameState.gameOver) {
			return
		}
		// If the game is hosted, execute the following systems
		if (scene.gameData.isHost) {
			scene.userInputSystem(scene.gm.world) // Process user input
			scene.checkPlayerOverlap(); // make sure players don't overlap
			scene.ballSystem(scene.gm.world) // Update ball system
			scene.updateBall() // Update ball state
			scene.npcSystem(scene.gm.world) // Process NPC behaviors
			scene.updateTimer()
		}

		// Always execute these systems regardless of host status
		scene.entitySystem(scene.gm.world) // Update entity system
		scene.movementSystem(scene.gm.world) // Update movement system

		// Update the scoreboard text with the current score
		scene.updateScoreBoard()
	}

	updatePause() {
		//scene.goalSystem()
	}
}
// Extended scene class with mixins applied
export default class GameScene extends BaseGameScene { }
applyMixins(GameScene, [SportsSceneMixin])
