## Entity Component System
### [Example Demo](https://morabaraba.gitlab.io/nibecs?game=solar)
### [Docs](https://morabaraba.gitlab.io/nibecs/docs/)

Modern JavaScript Example using [bitECS](https://github.com/NateTheGreatt/bitECS).
Source code in [`public/js`](./public/js),
see [modules](#javascript-modules) section below for import modules map.

## Resources

- https://github.com/NateTheGreatt/bitecs
- https://github.com/ourcade/phaser3-bitecs-getting-started
- https://phaser.discourse.group/t/composing-game-objects-kinda-like-ecs/1676/5
- http://craftyjs.com/documentation/

## Component list(TODO)

[/u/Tjakka5 suggested component list:](https://old.reddit.com/r/gamedev/comments/15vy509/ecs_a_pointless_overcomplication/jwyqok4/)

1. Position
1. Sprite (Replaces Tile)
1. Blocking
1. Opaque
1. Name
1. ViewShed (Combines with KnownShed)
1. Action
1. Health (Combines with BaseHealth and Dead)
1. Stamina (Combines with BaseStamina)
1. Controllable (Takes in commands to control the entity)
1. UserInputControls (Uses Controllable. replaces Player)
1. AIControls (Uses Controllable, replaces AI)

## JavaScript Modules

See [Importing modules using import maps(MDN JavaScript modules Guide)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules#importing_modules_using_import_maps).

Import map can be found in [`public/index.html`](./public/index.html)

## Co-pilot(Bing ChatGPT)

Certainly! Let’s break down the [provided JavaScript source](./public/js/index.mjs) in terms of **Data-Oriented Design (DOD)** principles, as explained in the book [**Data-Oriented Design**](https://www.dataorienteddesign.com/dodbook/) by Richard Fabian.

#### Importing Dependencies:

- The code starts by importing necessary dependencies from the `bitecs` library.
- **DOD** emphasizes understanding the data and hardware, which aligns with the need for efficient data manipulation libraries like `bitecs`.

#### Creating a World:

- The `createWorld()` function initializes a world context for managing entities and components.
- In **DOD**, the world represents the global state and data storage.

#### Adding Proxies and Components

- Proxies (`PositionProxy` and `VelocityProxy`) are created to efficiently access component data.
- Components (`Position` and `Velocity`) are added to the world for an entity (`eid`).
- **DOD** focuses on organizing data into components, allowing for cache-friendly access patterns.

#### Setting Component Data

- The `Velocity.x` and `Velocity.y` components are set for the specified entity.
- **DOD** emphasizes data manipulation over object-oriented abstractions.

#### System Pipeline

- The `pipeline` function combines two systems (`movementSystem` and `timeSystem`) to process the world.
- **DOD** encourages breaking down functionality into separate systems that operate on data.

#### Loop and Execution

- The `setInterval` loop repeatedly executes the pipeline function.
- **DOD** emphasizes performance and efficiency, which aligns with the need for consistent, predictable execution.

### In summary, this code snippet demonstrates several **DOD** principles

- **Component-Based Design**: Components (`Position` and `Velocity`) are used to represent data.
- **System Composition**: The `pipeline` combines multiple systems for processing.
- **Efficient Data Access**: Proxies and direct component access optimize data retrieval.
- **Focus on Data**: The code prioritizes data manipulation over object-oriented abstractions.

By adhering to these principles, the code aligns with the core concepts of **Data-Oriented Design**. 🚀

