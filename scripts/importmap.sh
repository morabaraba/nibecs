#!/bin/bash
scriptdir=`dirname $0`
mjsdir=../public
cd $scriptdir
# Find all the .mjs files in the current directory and subdirectories
files=$(find $mjsdir -name "*.mjs")
# Start the script tag for the importmap
echo "{ \"imports\": {"
# Initialize the comma variable to empty
comma=" "
# Loop through the files and extract the key and value for the importmap
for file in $files; do
  # Remove the leading ./ from the file path
  value=${file#$mjsdir/}
  # Remove the .mjs extension from the file path
  key=${value%.mjs}
  # Remove the js/ prefix from the key
  key=${key#js/}
  echo "\t$comma\"$key\": \"./$value\""
  # Set the comma variable to a comma for the next iteration
  comma=","
done
# End the script tag for the importmap
echo "} }"