#!/bin/bash
scriptdir=$(dirname "$0")
index="$scriptdir/../public/index.html"
# Capture the output of importmap.sh
importmap_output=$("$scriptdir/importmap.sh")
# Escape newlines for sed
escaped_output=$(echo "$importmap_output" | sed ':a;N;$!ba;s/\n/\\n/g')
# Use sed to replace the script tag content
sed -i -e "/<script type=\"importmap\">/,/<\/script>/c\<script type=\"importmap\">$escaped_output</script>" "$index"
