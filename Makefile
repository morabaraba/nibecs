PHONY: index docs
index:
	./scripts/sedimportmap.sh
docs:
	jsdoc -c jsdoc.json -d public/docs public/js/**
	sed -i 's/<h1 class="page-title">Home<\/h1>/<h1 class="page-title">nibECS<\/h1>/' public/docs/index.html
default: index docs